package com.fortunepay.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.interfaces.DialogClickInterface
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.SingletonManager
import com.fortunepay.views.activities.BankLoginActivity
import com.fortunepay.views.activities.DashboardActivity
import com.fortunepay.views.activities.LoginActivity
import com.fortunepay.views.activities.RegisterActivity

open class BaseActivity : AppCompatActivity() {

    var os: SingletonManager? = null
    var objt: ObjectSingleton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun launchActivity(act: Activity, moveTo: ActivityType) {
        var intent = Intent(act, DashboardActivity::class.java)
        when(moveTo) {
            ActivityType.LOGIN -> {
                intent = Intent(act, LoginActivity::class.java)
            }
            ActivityType.REGISTER -> {
                intent = Intent(act, RegisterActivity::class.java)
            }
            ActivityType.DASHBOARD -> {
                // do nothing
            }
            ActivityType.BANK_LOGIN -> {
                intent = Intent(act, BankLoginActivity::class.java)
            }
        }

        act.startActivity(intent)
    }

    fun showErrorMsg(msg: String, dialog: AppCompatDialog? = null) =
        DialogFactory.createOkErrorDialog(this, msg, object : DialogClickInterface {
            override fun onPositiveButtonClick() {
                dialog?.dismiss()
            }

            override fun onNegativeButtonClick() {

            }
        }).show()


}