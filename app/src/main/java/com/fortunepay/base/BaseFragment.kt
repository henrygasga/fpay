package com.fortunepay.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.fortunepay.interfaces.DialogClickInterface
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.ScreenType
import com.fortunepay.utils.SingletonManager
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*

open class BaseFragment : Fragment() {
    protected lateinit var mContext: Context
    protected lateinit var mActivity: Activity
    var os = ObjectSingleton
    var om: SingletonManager? = null
    lateinit var thisActivity: AppCompatActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        mActivity = context as Activity
    }

    fun initActionsOnClickEvents() {
        ivMenu.setOnClickListener {
            (activity as DashboardActivity).dlHome.openDrawer(GravityCompat.END)
        }
        ivPhotoDisplay.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }
        llUserInfo.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }

        ivBackButton.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.TRANSACTION_HISTORY)
        }
    }

    fun showErrorMsg(msg: String, dialog: AppCompatDialog? = null) =
        DialogFactory.createOkErrorDialog(thisActivity, msg, object : DialogClickInterface {
            override fun onPositiveButtonClick() {
                dialog?.dismiss()
            }

            override fun onNegativeButtonClick() {

            }
        }).show()
}