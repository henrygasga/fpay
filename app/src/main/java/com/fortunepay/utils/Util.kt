package com.fortunepay.utils

import android.content.Context
import android.net.ConnectivityManager
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fortunepay.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class Util {
    companion object {

        val DATE_FORMAT_DAY_MONTH_YEAR = "dd MMM yyyy"

        @JvmStatic
        fun isOnline(cContext: Context): Boolean {
            try {
                val cm = cContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = cm.activeNetworkInfo
                if (netInfo != null && netInfo.isConnectedOrConnecting) {
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return false
        }


        fun getDateToShow(timestamp: Long, format: String): String {
            var timestamp = timestamp
            if (timestamp > 0) {
                if (timestamp < 100000000000L) {
                    timestamp *= 1000
                }
                val sdf = SimpleDateFormat(format, Locale.ENGLISH)
                return sdf.format(timestamp)
            } else {
                return ""
            }

        }

        fun loadNormalImage(requestManager: RequestManager, file: File, v: AppCompatImageView) {
            val profile = RequestOptions()
                /*.placeholder(R.drawable.ic_ride_complete)*/
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
            /*.error(R.drawable.ic_ride_complete);*/
            requestManager.load(file)
                .apply(profile)
                .into(v)
        }

        fun loadProfileImage(requestManager: RequestManager, URL: File, v: AppCompatImageView) {

            val profile = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(CircleTransform())
                .placeholder(R.drawable.ic_vector_defaut_photo)
                .error(R.drawable.ic_vector_defaut_photo)

            requestManager
                .load(URL)
                .apply(profile)
                .into(v)
        }
    }
}