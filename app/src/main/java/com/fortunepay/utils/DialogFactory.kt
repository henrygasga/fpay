package com.fortunepay.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.interfaces.DialogClickInterface
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView

class DialogFactory {

    companion object {

        lateinit var dialog: AppCompatDialog

        @JvmStatic
        fun createProgressDialog(
            context: Context
        ): AppCompatDialog {

            val v: View = LayoutInflater.from(context).inflate(R.layout.loading_layout, null)
            val dialog = AppCompatDialog(context)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(v)
            return dialog
        }

        @JvmStatic
        fun createOkErrorDialog(context: Context, message: String, clickListener: DialogClickInterface) : AppCompatDialog {
            val v: View = LayoutInflater.from(context).inflate(R.layout.layout_dialog_ok, null)
            val dialog = AppCompatDialog(context)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)

            val btnOk = v.findViewById<View>(R.id.btn_ok_dialog) as  MaterialButton
            val textMessage = v.findViewById<View>(R.id.mtv_dialog_message) as MaterialTextView

            textMessage.text = message
            btnOk.setOnClickListener {
                dialog.dismiss()
                clickListener.onPositiveButtonClick()
            }

            dialog.setContentView(v)
            return dialog
        }
    }
}