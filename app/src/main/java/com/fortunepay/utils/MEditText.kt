package com.fortunepay.utils

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText

class MEditText(context: Context?, attrs: AttributeSet?) : TextInputEditText(context, attrs) {
    init {
        FontManager.applyFont(this, attrs)
    }
}