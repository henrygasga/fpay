package com.fortunepay.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.fortunepay.R
import com.fortunepay.interfaces.OtpInterface

class OtpView(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    var otpViewArray = arrayListOf<MTextView>()
    var otpBuilder = StringBuilder()


    init {
        val mInflater = getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mInflater.inflate(R.layout.otp_view, this)
    }

    companion object {
        lateinit var otpInterface: OtpInterface
    }


}