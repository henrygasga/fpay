package com.fortunepay.utils

enum class ScreenType {
    HOME,
    PROFILE,
    TRANSACTION_HISTORY,
    TRANSACTION_DETAILS,
    NOTIFICATION,
    REWARDS,
    SAVE_BANKS,
    SAVE_ACCOUNTS_PAGE,
    INPUT_AMOUNT
}