package com.fortunepay.utils

enum class ActivityType {
    BANK_LOGIN,
    DASHBOARD,
    LOGIN,
    REGISTER
}