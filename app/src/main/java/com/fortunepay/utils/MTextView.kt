package com.fortunepay.utils

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textview.MaterialTextView

class MTextView(context: Context, attrs: AttributeSet?) : MaterialTextView(context, attrs) {
    init {
        FontManager.applyFont(this, attrs)
    }
}