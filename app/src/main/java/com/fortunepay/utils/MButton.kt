package com.fortunepay.utils

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.button.MaterialButton

class MButton(context: Context, attrs: AttributeSet?) : MaterialButton(context, attrs) {
    init {
        FontManager.applyFont(this, attrs)
    }
}