package com.fortunepay.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.security.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class SecurePreferences {
    private class SecurePreferencesException internal constructor(e: Throwable) : RuntimeException(e)

    private val TRANSFORMATION = "AES/CBC/PKCS5Padding"
    private val KEY_TRANSFORMATION = "AES/ECB/PKCS5Padding"
    private val SECRET_KEY_HASH_TRANSFORMATION = "SHA-256"
    private val CHARSET = "UTF-8"
    private val encryptKeys: Boolean? = null
    private var writer: Cipher? = null
    private var reader: Cipher? = null
    private var keyWriter: Cipher? = null
    private var preferences: SharedPreferences? = null
    private var instance: SecurePreferences? = null

    @Throws(SecurePreferencesException::class)
    private fun SecurePreferences(
        context: Context, preferenceName: String, secureKey: String, encryptKeys: Boolean
    ): SecurePreferences? {
        try {
            writer = Cipher.getInstance(TRANSFORMATION)
            reader = Cipher.getInstance(TRANSFORMATION)
            keyWriter = Cipher.getInstance(KEY_TRANSFORMATION)
            initCiphers(secureKey)
            preferences = context.getSharedPreferences(preferenceName,Context.MODE_PRIVATE)
        }catch (ex: Exception) {
            when(ex) {
                is GeneralSecurityException, is UnsupportedEncodingException -> {
                    throw SecurePreferencesException(ex)
                }
                else -> {
                    throw ex
                }
            }
        }
        return this
    }

    fun getInstance(contxt: Context, prefName: String): SecurePreferences {
        if (instance == null) {
            instance = SecurePreferences(contxt, prefName, prefName+"key",true)
        }
        return instance!!
    }

    @Throws(
        UnsupportedEncodingException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        InvalidAlgorithmParameterException::class
    )
    private fun initCiphers(secureKey: String) {
        val ivSpec = getIv()
        val secretKey = getSecretKey(secureKey)

        writer!!.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec)
        reader!!.init(Cipher.DECRYPT_MODE, secretKey, ivSpec)
        keyWriter!!.init(Cipher.ENCRYPT_MODE, secretKey)
    }

    private fun getIv(): IvParameterSpec {
        val iv = ByteArray(writer!!.blockSize)
        System.arraycopy(
            "fldsjfodasjifudslfjdsaofshaufihadsf".toByteArray(), 0,
            iv, 0, writer!!.blockSize
        )
        return IvParameterSpec(iv)
    }

    @Throws(UnsupportedEncodingException::class, NoSuchAlgorithmException::class)
    private fun getSecretKey(key: String): SecretKeySpec {
        val keyBytes = createKeyBytes(key)
        return SecretKeySpec(keyBytes, TRANSFORMATION)
    }

    @Throws(UnsupportedEncodingException::class, NoSuchAlgorithmException::class)
    private fun createKeyBytes(key: String): ByteArray {
        val md = MessageDigest
            .getInstance(SECRET_KEY_HASH_TRANSFORMATION)
        md.reset()
        return md.digest(key.toByteArray(charset(CHARSET)))
    }

    fun putString(key: String, value: String?) {
        if (value == null) {
            preferences!!.edit().remove(toKey(key)).apply()
        } else {
            putValue(toKey(key), value)
        }
    }

    fun putBoolean(key: String, value: Boolean?) {
        if (value == null) {
            preferences!!.edit().remove(toKey(key)).apply()
        } else {
            putValue(toKey(key), java.lang.Boolean.toString(value))
        }
    }

    fun putLong(key: String, value: Long) {

        putValue(toKey(key), java.lang.Long.toString(value))

    }

    fun putInt(key: String, value: Int) {

        putValue(toKey(key), Integer.toString(value))

    }

    fun containsKey(key: String): Boolean {
        return preferences!!.contains(toKey(key))
    }

    fun removeValue(key: String) {
        preferences!!.edit().remove(toKey(key)).apply()
    }

    @Throws(SecurePreferencesException::class)
    fun getString(key: String, value: String): String {
        if (preferences!!.contains(toKey(key))) {
            val securedEncodedValue = preferences!!.getString(toKey(key), "")
            return decrypt(securedEncodedValue!!)
        }
        return value
    }

    @Throws(SecurePreferencesException::class)
    fun getLong(key: String, value: Long): Long {
        if (preferences!!.contains(toKey(key))) {
            val securedEncodedValue = preferences!!.getString(toKey(key), "")
            return java.lang.Long.parseLong(decrypt(securedEncodedValue!!))
        }
        return value
    }

    @Throws(SecurePreferencesException::class)
    fun getBoolean(key: String, value: Boolean): Boolean {
        if (preferences!!.contains(toKey(key))) {
            val securedEncodedValue = preferences!!.getString(toKey(key), "")
            return java.lang.Boolean.parseBoolean(decrypt(securedEncodedValue!!))
        }
        return value
    }

    @Throws(SecurePreferencesException::class)
    fun getInt(key: String, value: Int): Int {
        if (preferences!!.contains(toKey(key))) {
            val securedEncodedValue = preferences!!.getString(toKey(key), "")
            return Integer.parseInt(decrypt(securedEncodedValue!!))
        }
        return value
    }

    fun commit() {
        preferences!!.edit().apply()
    }

    fun clear() {
        preferences!!.edit().clear().apply()
    }

    private fun toKey(key: String): String {
        return if (this.encryptKeys!!)
            encrypt(key, keyWriter!!)
        else
            key
    }

    @Throws(SecurePreferencesException::class)
    private fun putValue(key: String, value: String) {
        val secureValueEncoded = encrypt(value, writer!!)

        preferences!!.edit().putString(key, secureValueEncoded).apply()
    }

    @Throws(SecurePreferencesException::class)
    private fun encrypt(value: String, writer: Cipher): String {
        val secureValue: ByteArray
        try {
            secureValue = convert(writer, value.toByteArray(charset(CHARSET)))
        } catch (e: UnsupportedEncodingException) {
            throw SecurePreferencesException(e)
        }

        return Base64.encodeToString(
            secureValue,
            Base64.NO_WRAP
        )
    }

    private fun decrypt(securedEncodedValue: String): String {
        val securedValue = Base64
            .decode(securedEncodedValue, Base64.NO_WRAP)
        val value = convert(reader!!, securedValue)
        try {
            return String(value, charset(CHARSET))
        } catch (e: UnsupportedEncodingException) {
            throw SecurePreferencesException(e)
        }

    }

    @Throws(SecurePreferencesException::class)
    private fun convert(cipher: Cipher, bs: ByteArray): ByteArray {
        try {
            return cipher.doFinal(bs)
        } catch (e: Exception) {
            throw SecurePreferencesException(e)
        }

    }
}