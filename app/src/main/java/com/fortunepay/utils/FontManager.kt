package com.fortunepay.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.widget.TextView
import com.fortunepay.R
import java.util.HashMap

open class FontManager {

    companion object {

        private val mFontCache = HashMap<String, Typeface>()
        fun applyFont(textView: TextView, attrs: AttributeSet?) {
            if (attrs != null) {
                val context = textView.context //getting context from the view
                val styledAttributes = context.theme.obtainStyledAttributes(attrs, R.styleable.customFont, 0, 0)
                val fontPath = styledAttributes.getString(R.styleable.customFont_fontFamily) // getting the attribute
                if (!TextUtils.isEmpty(fontPath)) {
                    val typeface = fontPath?.let { getTypeface(context, it) }
                    if (typeface != null) {
                        textView.typeface = typeface //applying font to the textview
                    }
                }
                styledAttributes.recycle()
            }
        }
        @Throws(RuntimeException::class)
        fun getTypeface(context: Context, path: String): Typeface? {
            var typeface = mFontCache[path]
            if (typeface == null) {
                try {
                    typeface = Typeface.createFromAsset(context.assets, path)
                } catch (exception: RuntimeException) {
                    val message = "Font assets/$path cannot be loaded"
                    throw RuntimeException(message)
                }

                mFontCache[path] = typeface
            }
            return typeface
        }

        @Throws(RuntimeException::class)
        fun getTypeface(context: Context, pathResId: Int): Typeface? {
            try {
                val path = context.resources.getString(pathResId)
                return getTypeface(context, path)
            } catch (exception: Resources.NotFoundException) {
                val message = "String resource id $pathResId not found"
                throw RuntimeException(message)
            }
        }

    }


}