package com.fortunepay.utils

enum class SelectionType {
    NATURE_OF_WORK,
    IDENTIFICATION
}