package com.fortunepay.utils

import android.content.Context
import android.content.SharedPreferences
import com.fortunepay.R
import com.fortunepay.model.User

class SingletonManager private constructor(context: Context){

    private var pref: SharedPreferences?    = null

    init {
        val prefName = context.getString(R.string.app_name)
        pref = context.getSharedPreferences(prefName,Context.MODE_PRIVATE)
    }

    fun getPrefByKey(key: String, value: Boolean): Boolean{
        return if(pref!!.contains(key)){
            pref!!.getBoolean(key, value)
        } else {
            value
        }
    }

    fun setPrefByKey(key: String, value: Boolean) {
        val editor = pref!!.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getPrefByKey(key: String, value: String): String? {
        return if(pref!!.contains(key)){
            pref!!.getString(key, value)
        } else {
            value
        }
    }

    fun setPrefByKey(key: String, value: String) {
        val editor = pref!!.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun saveUser(user: User){
        setPrefByKey(Const.KEY_USER_ID, user.user_id!!)
        setPrefByKey(Const.KEY_USER_FIRST_NAME, user.first_name!!)
        setPrefByKey(Const.KEY_USER_LAST_NAME, user.last_name!!)
        setPrefByKey(Const.KEY_USER_DIAL_CODE, user.dial_code!!)
        setPrefByKey(Const.KEY_USER_PHONE, user.phone_number!!)
        setPrefByKey(Const.KEY_USER_EMAIL, user.email!!)
        setPrefByKey(Const.KEY_USER_IMAGE, user.image!!)
        setPrefByKey(Const.KEY_USER_TYPE, user.user_type!!)
        setPrefByKey(Const.KEY_IS_PHONE_VERIFIED, user.is_phone_verified!!)
//        setPrefByKey(Const.KEY_CURRENCY_SYMBOL, user.currency.sysmbol)
//        setPrefByKey(Const.KEY_QR_CODE_STRING, user.qr_code.qr_code_string)
        setPrefByKey(Const.KEY_TRANSACTION_PIN, user.login_pin!!)
//        setPrefByKey(Const.KEY_IS_TOP_UP_FOR_OTHER_STATUS,user.top_up_request_status)
    }

    companion object : SingletonHolder<SingletonManager, Context>(::SingletonManager)
}