package com.fortunepay.utils

class Const {
    companion object {
        var BASE_URL: String                        = "http://devapi.fortunepay.com.ph/v1/"
        var COGNITO_POOL_ID: String                 = "ap-southeast-1:4141a943-3cd4-4542-889e-68ba2948315e"
        var AWS_BUCKET_NAME: String                 = "fortunepaydocs"

        //API ENDPOINTS
        var LOGIN: String                           = "customer/login"
        var PHONE_REGISTER: String                  = "customer/phoneregister"
        var VERIFY_OTP: String                      = "customer/verifyotpsignin"
        var VERIFY_OTP_SIGN_UP: String              = "customer/verifyotpforsingup"
        var CREATE_LOGIN_PIN: String                = "customer/createloginpin"
        var BASIC_PROFILE: String                   = "customer/completeprofile"
        var VERIFICATION_ID_FIRST: String           = "customer/documentidverification"
        var DOCUMENT_LIST: String                   = "customer/getdocumentlist"
        var RESEND_OTP: String                      = "customer/resendotp"
        var VERIFY_PIN: String                      = "customer/verifyloginpin"
        var WALLET_BALANCE_API: String              = "wallet/getwalletbalance"
        var SCAN_BY_USER_ID: String                 = "wallet/scanqrcode"

        //SELECTION CODE
        var SELECTION_TYPE: String                  = "SELECTION_TYPE"
        var SELECTED_DATA: String                   = "SELECTED_DATA"
        var FILE_PATH: String                       = "FILE_PATH"
        var NATURE_OF_WORK: Int                     = 334
        var IDENTIFICATION: Int                     = 335
        var PHOTO_FILE_URL: Int                     = 336
        var DISPLAY_PHOTO_FILE: Int                 = 337


        var COMPANY_ID: String                      = "4bf735e97269421a80b82359e7dc2288"

        // HEADER STRING
        var APPLICATION_FORM_ENCODED                = "application/x-www-form-urlencoded"

        //QR TYPE
        val SHOW_QR_TYPE_MY_QR = 1
        val SHOW_QR_TYPE_RECEIVE_PAYMENT = 2
        val SHOW_QR_TYPE_FORTUNE_PAY_OVER_THE_COUNTER = 3

        //SHARED PREFERENCES KEYs
        var KEY_INTRO: String                       = "KEY_INTRO"
        var KEY_MOBILE_NUMBER: String               = "KEY_MOBILE_NUMBER"
        var KEY_DEVICE_ID: String                   = "KEY_DEVICE_ID"
        var KEY_TOKEN: String                       = "KEY_TOKEN"
        var KEY_USER_ID: String                     = "KEY_USER_ID"
        var KEY_USER_FIRST_NAME: String             = "KEY_USER_FIRST_NAME"
        var KEY_USER_IMAGE: String                  = "KEY_USER_IMAGE"
        var KEY_USER_EMAIL: String                  = "KEY_USER_EMAIL"
        var KEY_USER_DIAL_CODE: String              = "KEY_USER_DIAL_CODE"
        var KEY_USER_PHONE: String                  = "KEY_USER_PHONE"
        var KEY_USER_TYPE: String                   = "KEY_USER_TYPE"
        var KEY_USER_LAST_NAME: String              = "KEY_USER_LAST_NAME"
        var KEY_IS_PHONE_VERIFIED: String           = "KEY_IS_PHONE_VERIFIED"
        var KEY_CURRENCY_SYMBOL: String             = "KEY_CURRENCY_SYMBOL"
        var KEY_QR_CODE_STRING: String              = "KEY_QR_CODE_STRING"
        var KEY_TRANSACTION_PIN: String             = "KEY_TRANSACTION_PIN"
        var KEY_IS_TOP_UP_FOR_OTHER_STATUS: String  = "KEY_IS_TOP_UP_FOR_OTHER_STATUS"

    }
}