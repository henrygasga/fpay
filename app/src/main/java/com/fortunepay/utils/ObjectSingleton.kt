package com.fortunepay.utils

import com.fortunepay.model.NatureOfWork
import com.fortunepay.model.Token
import com.fortunepay.model.User
import java.io.File

object ObjectSingleton {
    var userLogin: User? = null
    var userTransaction: User? = null
    var token: Token? = null
    var getSelection: SelectionType? = null
    var identificationCard: NatureOfWork? = null
    var displayPicFile: File? = null
}