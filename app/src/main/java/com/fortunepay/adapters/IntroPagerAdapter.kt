package com.fortunepay.adapters

import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.ArrayList

class IntroPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val titleList = ArrayList<String>()
    private val fragmentList = ArrayList<Fragment>()


    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
    }

    fun addFragment(title: String, fragment: Fragment) {
        titleList.add(title)
        fragmentList.add(fragment)
    }

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    @Nullable
    override fun getPageTitle(position: Int): CharSequence {
        return titleList[position]
    }
}