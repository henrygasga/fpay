package com.fortunepay.interfaces

import com.fortunepay.utils.ActivityType

interface ScreenChangeListener {
    fun onChange(type: ActivityType)
}