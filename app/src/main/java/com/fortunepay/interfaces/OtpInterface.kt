package com.fortunepay.interfaces

interface OtpInterface {
    fun onOTPEntered(makeButtonEnable: Boolean)
}