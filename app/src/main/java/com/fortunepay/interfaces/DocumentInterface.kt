package com.fortunepay.interfaces

import com.fortunepay.model.ResponseDocuList

interface DocumentInterface {
    interface Base {
        fun onFailed(msg: String)
    }
    interface DocuList : Base {
        fun onSuccessGetDocu(res: ResponseDocuList)
    }
    interface Presenter {
        fun getDocuList(comId: String)
    }
}