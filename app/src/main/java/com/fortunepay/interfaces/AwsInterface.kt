package com.fortunepay.interfaces

import java.io.File

interface AwsInterface {
    interface View {
        fun onSuccessfullyUploaded(url: String)
        fun onAwsError()
    }
    interface PhotoUpload {
        fun onSuccessfullyUploaded(url: String)
        fun onAwsError()
    }
    interface Presenter {
        fun doUpload(mFile: File)
    }
}