package com.fortunepay.interfaces

interface DialogClickInterface {
    fun onPositiveButtonClick()
    fun onNegativeButtonClick()
}