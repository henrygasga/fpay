package com.fortunepay.interfaces

import com.fortunepay.model.*
import com.fortunepay.requestModel.*
import com.fortunepay.utils.ActivityType

interface AccountInterface {
    interface Base {
        fun onFailed(msg: String)
    }
    interface View {
        fun loginSuccess(baseResponse: BaseResponse)
        fun loginFailed(msg: String)
    }
    interface OtpSignIn {
        fun onReceived(baseResponse: BaseResponse, act: ActivityType)
        fun verifyFailed(msg: String)
        fun otpResend(baseOtp: BaseResendOtp)
    }
    interface PinVerify {
        fun onVerified(body: ResponseData)
        fun onFailedVerify(msg: String)
    }
    interface scanById {
        fun onFailed(msg: String)
        fun onSuccess(act: User)
    }
    interface phoneRegister {
        fun onRegisterFailed(msg: String)
        fun onRegisterSuccess(baseResponse: BaseResponse)
    }
    interface BasicProfileRegistration {
        fun onBasicFailed(msg: String)
        fun onBasicSuccess(baseResponse: ResponseData)
    }
    interface SemiDetailsView : Base {
        fun onSemiDetailsSuccess(baseResponse: ResponseData)
    }
    interface SemiTwoDetailsView : Base {
        fun onSemiTwoDetailsSuccess(baseResponse: ResponseData)
    }
    interface SemiThreeDetailsView : Base {
        fun onSemiThreeDetailsSuccess(baseResponse: ResponseData)
    }
    interface AccountFinalProcessView : Base {
        fun onFinalSuccess(baseResponse: ResponseData)
    }
    interface Presenter {
        fun login(data: LoginModel)
        fun verifyOtp(otp: OtpModel, act: ActivityType)
        fun resentOtp(resendType: Int, compId: String, token: String)
        fun verifyPin(compId: String,loginPin: String, dCode: String, pNumber: String)
        fun scanQRByUserId(comId: String,cosId: String)
        fun phoneRegister(data: LoginModel)
        fun createPin(compId: String,loginPin: String, device_id: String, device_type: String)
        fun basicProfile(basicProfile: BasicProfile)
        fun semiDetails(semDetails: SemiDetails)
        fun semiTwoDetails(semTwoDetails: SemiTwoDetails)
        fun semiThreeDetails(semThreeDetails: SemiTwoDetails, docSelfie: String)
        fun accountFinalProcess(compId: String, wccn: String, vcn: String)
    }
}