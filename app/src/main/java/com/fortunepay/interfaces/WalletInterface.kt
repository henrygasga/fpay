package com.fortunepay.interfaces

import com.fortunepay.model.WalletData

interface WalletInterface {
    interface View{
        fun onBalanceSuccess(wallet: WalletData)
        fun onBalanceFailed(msg: String)
    }
    interface Presenter{
        fun getBalance(comId: String)
    }
}