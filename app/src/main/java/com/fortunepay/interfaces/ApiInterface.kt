package com.fortunepay.interfaces

import com.fortunepay.model.*
import kotlinx.coroutines.Deferred
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    //LOGIN ENDPOINTS
    @FormUrlEncoded
    @POST
    fun login(
        @Url urlEndpoint: String,
        @Field("dial_code") dial_code: String,
        @Field("phone_number") phone_number: String,
        @Field("device_info") device_info: String,
        @Field("device_type") device_type: String,
        @Field("device_id") device_id: String,
        @Field("company_id") company_id: String,
        @Field("send_otp") send_otp: String
    ) : Deferred<Response<BaseResponse>>

    @FormUrlEncoded
    @POST
    fun phoneRegister(
        @Url urlEndpoint: String,
        @Field("dial_code") dial_code: String,
        @Field("phone_number") phone_number: String,
        @Field("device_info") device_info: String,
        @Field("device_type") device_type: String,
        @Field("device_id") device_id: String,
        @Field("company_id") company_id: String
    ) : Deferred<Response<BaseResponse>>

    @FormUrlEncoded
    @POST
    fun verifyOtpLogin(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("device_info") device_info: String,
        @Field("device_type") device_type: String,
        @Field("device_id") device_id: String,
        @Field("company_id") company_id: String,
        @Field("otp") otp: String
    ) : Deferred<Response<BaseResponse>>

    @FormUrlEncoded
    @POST
    fun resendOtp(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("resend_type") resend_type: Int
    ) : Deferred<Response<BaseResendOtp>>

    @FormUrlEncoded
    @POST
    fun verifyPin(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("login_pin") login_pin: String,
        @Field("dial_code") dial_code: String,
        @Field("phone_number") phone_number: String
    ): Deferred<Response<ApiResponse>>

    //Wallet ENDPOINTS
    @FormUrlEncoded
    @POST
    fun walletBalance(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String
    ): Deferred<Response<WalletResponse>>

    @FormUrlEncoded
    @POST
    fun scanById(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("customer_id") customer_id: String
    ): Deferred<Response<AccountReponse>>

    @FormUrlEncoded
    @POST
    fun createPin(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("login_pin") login_pin: String,
        @Field("device_id") device_id: String,
        @Field("device_type") device_type: String
    ): Deferred<Response<ApiResponse>>

    @FormUrlEncoded
    @POST
    fun basicProfile(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("first_name") first_name: String,
        @Field("middle_name") middle_name: String,
        @Field("last_name") last_name: String,
        @Field("birth_date") birth_date: String,
        @Field("email") email: String,
        @Field("image") image: String,
        @Field("location_lat") location_lat: String,
        @Field("location_lng") location_lng: String
    ): Deferred<Response<ApiResponse>>

    @FormUrlEncoded
    @POST
    fun semiDetails(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("nationality") nationality: String
    ): Deferred<Response<ApiResponse>>

    @FormUrlEncoded
    @POST
    fun semiTwoDetails(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("source_of_income") source_of_income: String,
        @Field("nature_of_work_id") nature_of_work_id: String,
        @Field("nature_of_work") nature_of_work: String,
        @Field("company_name") company_name: String,
        @Field("document_id") document_id: String,
        @Field("document_number") document_number: String,
        @Field("document_image") document_image: String
    ): Deferred<Response<ApiResponse>>

    @FormUrlEncoded
    @POST
    fun getDocumentList(
        @Url urlEndpoint: String,
        @Field("company_id") company_id: String
    ): Deferred<Response<ResponseDocu>>

    @FormUrlEncoded
    @POST
    fun semiThreeDetails(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("document_selfie_front") document_selfie_front: String,
        @Field("document_id") document_id: String
    ): Deferred<Response<ApiResponse>>

    @FormUrlEncoded
    @POST
    fun finalProcess(
        @Url urlEndpoint: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String,
        @Field("company_id") company_id: String,
        @Field("we_chat_contact_number") we_chat_contact_number: String,
        @Field("viber_contact_number") viber_contact_number: String
    ): Deferred<Response<ApiResponse>>

}