package com.fortunepay.presenter

import android.content.Context
import android.util.Log
import com.fortunepay.R
import com.fortunepay.api.ApiFactory
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.model.ScanByUserId
import com.fortunepay.requestModel.*
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.Const
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.Util
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.io.StringReader
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

@Suppress("UNCHECKED_CAST")
class AccountPresenter(context: Context) : AccountInterface.Presenter, CoroutineScope {

    var ctx: Context = context
    val service = ApiFactory.api
    var viewAccount: AccountInterface.View? = null
    var viewOtpVerify: AccountInterface.OtpSignIn? = null
    var viewPinVerify: AccountInterface.PinVerify? = null
    var viewScanById: AccountInterface.scanById? = null
    var viewRegister: AccountInterface.phoneRegister? = null
    var viewBasic: AccountInterface.BasicProfileRegistration? = null
    var viewSemiDetails: AccountInterface.SemiDetailsView? = null
    var viewSemiTwoDetails: AccountInterface.SemiTwoDetailsView? = null
    var viewSemiThreeDetails: AccountInterface.SemiThreeDetailsView? = null
    var viewFinal: AccountInterface.AccountFinalProcessView? = null
    var os = ObjectSingleton

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun login(data: LoginModel) {
        GlobalScope.launch(Dispatchers.Main) {
            val loginRequest = service.login(
                    Const.LOGIN,
                    data.dialCode,
                    data.phoneNumber,
                    data.deviceInfo,
                    data.deviceType,
                    data.deviceId,
                    data.companyId,
                    data.sendOtp!!
            )
                val response = loginRequest.await()

                if (response.body()!!.success == 0) {
                    val msg = response.body()!!.error as ArrayList<String>
                    viewAccount?.loginFailed(msg[0])
                } else {
                    viewAccount?.loginSuccess(response.body()!!)
                }

//            try {
//
//            }catch (e: Exception){
//
//            }
        }
    }

    fun getApiEndpoint(act: ActivityType) : String {
        when(act) {
            ActivityType.LOGIN -> {
                return Const.VERIFY_OTP
            }
            ActivityType.REGISTER -> {
                return Const.VERIFY_OTP_SIGN_UP
            }
            else -> {
                return Const.VERIFY_OTP
            }
        }
    }

    override fun verifyOtp(otp: OtpModel, act: ActivityType) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val verifyOtp = service.verifyOtpLogin(
                    getApiEndpoint(act),
                    "application/x-www-form-urlencoded",
                    "Bearer ${otp.token}",
                    otp.deviceInfo,
                    otp.deviceType,
                    otp.deviceId,
                    otp.companyId,
                    otp.otp
                )

                try {
                    val response = verifyOtp.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewOtpVerify?.verifyFailed(msg[0])
                    } else {
                        viewOtpVerify?.onReceived(response.body()!!, act)
                    }
                } catch (e: Exception) {
                    viewOtpVerify?.verifyFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewScanById?.onFailed("No Interner Connection")
        }
    }

    override fun resentOtp(resendType: Int, compId: String, token: String) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val resendOtp = service.resendOtp(
                    Const.RESEND_OTP,
                    "application/x-www-form-urlencoded",
                    "Bearer $token",
                    compId,
                    resendType
                )

                try {
                    val response = resendOtp.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewOtpVerify?.verifyFailed(msg[0])
                    } else {
                        viewOtpVerify?.otpResend(response.body()!!)
                    }
                } catch (e: Exception) {
                    viewOtpVerify?.verifyFailed(e.message.toString())
                }
            }
        } else {
            viewOtpVerify?.verifyFailed("No Interner Connection")
        }
    }

    override fun verifyPin(compId: String, loginPin: String, dCode: String, pNumber: String) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val verifyPin = service.verifyPin(
                    Const.VERIFY_PIN,
                    "application/x-www-form-urlencoded",
                    "Bearer ${os.token?.token}",
                    compId,
                    loginPin,
                    dCode,
                    pNumber
                )

                try{
                    val response = verifyPin.await()
                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewPinVerify?.onFailedVerify(msg[0])
                    } else {
                        val body = response.body()!!.data
                        viewPinVerify?.onVerified(body)
                    }
                }catch (e: Exception){
                    viewPinVerify?.onFailedVerify(ctx.getString(R.string.something_wong))
                }

            }
        } else {
            viewPinVerify?.onFailedVerify("No Interner Connection")
        }
    }

    override fun scanQRByUserId(comId: String, cosId: String) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val verifyUserId = service.scanById(
                    Const.SCAN_BY_USER_ID,
                    Const.APPLICATION_FORM_ENCODED,
                    "Bearer ${os.token?.token}",
                    comId,
                    cosId
                )

                val response = verifyUserId.await()

                if (response.body()!!.success == 0) {
                    val msg = response.body()!!.error as ArrayList<String>
                    viewScanById?.onFailed(msg[0])
                } else {
                    try {
                        val body = response.body()!!.data.user
                        viewScanById?.onSuccess(body)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        viewScanById?.onFailed(ctx.getString(R.string.something_wong))
                    }
                }
            }
        } else {
            viewScanById?.onFailed("No Interner Connection")
        }
    }

    override fun phoneRegister(data: LoginModel) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val register = service.phoneRegister(
                    Const.PHONE_REGISTER,
                    data.dialCode,
                    data.phoneNumber,
                    data.deviceInfo,
                    data.deviceType,
                    data.deviceId,
                    data.companyId
                )

                try {
                    val response = register.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewRegister?.onRegisterFailed(msg[0])
                    } else {
                        viewRegister?.onRegisterSuccess(response.body()!!)
                    }


                } catch (e: Exception){
                    viewRegister?.onRegisterFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewRegister?.onRegisterFailed("No Internet Connection")
        }
    }

    override fun createPin(
        compId: String,
        loginPin: String,
        device_id: String,
        device_type: String
    ) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val createPin = service.createPin(
                    Const.CREATE_LOGIN_PIN,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    compId,
                    loginPin,
                    device_id,
                    device_type
                )

                try {
                    val response = createPin.await()
                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewPinVerify?.onFailedVerify(msg[0])
                    } else {
                        viewPinVerify?.onVerified(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewPinVerify?.onFailedVerify(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewPinVerify?.onFailedVerify("No Internet Connection")
        }
    }

    override fun basicProfile(basicProfile: BasicProfile) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {

                val basic = service.basicProfile(
                    Const.BASIC_PROFILE,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    basicProfile.company_id,
                    basicProfile.first_name,
                    basicProfile.middle_name,
                    basicProfile.last_name,
                    basicProfile.birth_date,
                    basicProfile.email,
                    basicProfile.image!!,
                    basicProfile.location_lat,
                    basicProfile.location_lng
                )

                try {
                    val response = basic.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewBasic?.onBasicFailed(msg[0])
                    } else {
                        viewBasic?.onBasicSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewBasic?.onBasicFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewBasic?.onBasicFailed("No Internet Connection")
        }
    }

    override fun semiDetails(semDetails: SemiDetails) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val semiDetails = service.semiDetails(
                    Const.VERIFICATION_ID_FIRST,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    semDetails.company_id,
                    semDetails.address,
                    semDetails.city,
                    semDetails.state,
                    semDetails.nationality
                )
                try {
                    val response = semiDetails.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewSemiDetails?.onFailed(msg[0])
                    } else {
                        viewSemiDetails?.onSemiDetailsSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewSemiDetails?.onFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewSemiDetails?.onFailed("No Internet Connection")
        }
    }

    override fun semiTwoDetails(semTwoDetails: SemiTwoDetails) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main) {
                val semiTwoDet = service.semiTwoDetails(
                    Const.VERIFICATION_ID_FIRST,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    semTwoDetails.company_id,
                    semTwoDetails.source_of_income,
                    semTwoDetails.nature_of_work_id,
                    semTwoDetails.nature_of_work,
                    semTwoDetails.company_name,
                    semTwoDetails.document_id,
                    semTwoDetails.document_number,
                    semTwoDetails.document_image
                )

                try {
                    val response = semiTwoDet.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewSemiTwoDetails?.onFailed(msg[0])
                    } else {
                        viewSemiTwoDetails?.onSemiTwoDetailsSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewSemiTwoDetails?.onFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewSemiTwoDetails?.onFailed("No Internet Connection")
        }
    }

    override fun semiThreeDetails(semThreeDetails: SemiTwoDetails, docSelfie: String) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val semiThreeDet = service.semiThreeDetails(
                    Const.VERIFICATION_ID_FIRST,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    semThreeDetails.company_id,
                    docSelfie,
                    semThreeDetails.document_id
                )


                try {
                    val response = semiThreeDet.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewSemiThreeDetails?.onFailed(msg[0])
                    } else {
                        viewSemiThreeDetails?.onSemiThreeDetailsSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewSemiThreeDetails?.onFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewSemiThreeDetails?.onFailed("No Internet Connection")
        }
    }

    override fun accountFinalProcess(compId: String, wccn: String, vcn: String) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val finalProcess = service.finalProcess(
                    Const.VERIFICATION_ID_FIRST,
                    "application/x-www-form-urlencoded",
                    "Bearer ${ObjectSingleton.token?.token}",
                    compId,
                    wccn,
                    vcn
                )
                try {
                    val response = finalProcess.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewFinal?.onFailed(msg[0])
                    } else {
                        viewFinal?.onFinalSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewFinal?.onFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewFinal?.onFailed("No Internet Connection")
        }
    }
}