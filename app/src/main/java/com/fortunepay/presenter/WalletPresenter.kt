package com.fortunepay.presenter

import android.content.Context
import com.fortunepay.api.ApiFactory
import com.fortunepay.interfaces.WalletInterface
import com.fortunepay.utils.Const
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.Util
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

@Suppress("UNCHECKED_CAST")
class WalletPresenter(context: Context) : WalletInterface.Presenter, CoroutineScope {

    var ctx: Context = context
    val service = ApiFactory.api
    var os = ObjectSingleton
    var viewBallance: WalletInterface.View? = null


    override fun getBalance(comId: String) {
        if(Util.isOnline(ctx)){
            GlobalScope.launch(Dispatchers.Main){
                try {
                    val walletReq = service.walletBalance(
                        Const.WALLET_BALANCE_API,
                        Const.APPLICATION_FORM_ENCODED,
                        "Bearer ${os.token!!.token}",
                        comId
                    )

                    val response = walletReq.await()

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewBallance?.onBalanceFailed(msg[0])
                    } else {
                        viewBallance?.onBalanceSuccess(response.body()!!.data.wallet)
                    }
                } catch (e: Exception) {
//                    viewBallance?.onBalanceFailed(e.message)
                }
            }
        } else {
            viewBallance?.onBalanceFailed("No Internet Connection")
        }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()



}