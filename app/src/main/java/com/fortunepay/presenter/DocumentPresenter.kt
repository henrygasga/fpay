package com.fortunepay.presenter

import android.content.Context
import android.util.Log
import com.fortunepay.R
import com.fortunepay.api.ApiFactory
import com.fortunepay.interfaces.DocumentInterface
import com.fortunepay.utils.Const
import com.fortunepay.utils.Util
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

@Suppress("UNCHECKED_CAST")
class DocumentPresenter(context: Context) : DocumentInterface.Presenter, CoroutineScope {

    var ctx: Context = context
    val service = ApiFactory.api
    var viewDocuList: DocumentInterface.DocuList? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun getDocuList(comId: String) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val docuList = service.getDocumentList(Const.DOCUMENT_LIST, comId)

                try {
                    val response = docuList.await()

                    Log.d("getDocuList","$response")
                    Log.d("getDocuList","${response.body()!!}")

                    if (response.body()!!.success == 0) {
                        val msg = response.body()!!.error as ArrayList<String>
                        viewDocuList?.onFailed(msg[0])
                    } else {
                        viewDocuList?.onSuccessGetDocu(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewDocuList?.onFailed(ctx.getString(R.string.something_wong))
                }
            }
        } else {
            viewDocuList?.onFailed("No Internet Connection")
    }
    }
}