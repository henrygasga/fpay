package com.fortunepay.aws

import android.content.Context
import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.fortunepay.utils.Const

class AwsClient {
    private var sS3Client: AmazonS3Client? = null
    private var sCredProvider: CognitoCachingCredentialsProvider? = null
    private var sTransferUtility: TransferUtility? = null

    private fun getCredProvider(context: Context): CognitoCachingCredentialsProvider {
        if (sCredProvider == null) {
            sCredProvider = CognitoCachingCredentialsProvider(
                context.applicationContext,
                Const.COGNITO_POOL_ID,
                Regions.AP_SOUTHEAST_1
            )
        }
        return sCredProvider!!
    }

    fun getS3Client(context: Context): AmazonS3Client {
        if (sS3Client == null) {
            val clientConfiguration = ClientConfiguration()
            clientConfiguration.connectionTimeout = 30 * 10000
            clientConfiguration.socketTimeout = 30 * 10000
            sS3Client =
                AmazonS3Client(getCredProvider(context.applicationContext), clientConfiguration)
        }
        return sS3Client!!
    }

    fun getTransferUtility(context: Context): TransferUtility {
        if (sTransferUtility == null) {
            //            sTransferUtility = new TransferUtility(getS3Client(context.getApplicationContext()),
            //                    context.getApplicationContext());
            sTransferUtility = TransferUtility.builder()
                .context(context.applicationContext)
                .awsConfiguration(AWSMobileClient.getInstance().configuration)
                .s3Client(getS3Client(context.applicationContext))
                .build()
        }

        return sTransferUtility!!
    }
}