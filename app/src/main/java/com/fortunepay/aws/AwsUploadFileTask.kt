package com.fortunepay.aws

import android.content.Context
import android.util.Log
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.fortunepay.interfaces.AwsInterface
import com.fortunepay.utils.Const
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.Util
import kotlinx.coroutines.*
import java.io.File
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class AwsUploadFileTask(ctx: Context, listener: AwsInterface.View? = null ) : AwsInterface.Presenter,
    CoroutineScope {

    var awsUploadFileListener: AwsInterface.View? = null
    var awsPhotoUploadView: AwsInterface.PhotoUpload? = null
    var mContext: Context? = null
    var awsClient: AwsClient? = null
    var mTransferUtility: TransferUtility? = null
    var mTransferObserver: TransferObserver? = null

    init {
        awsUploadFileListener = listener
        mContext = ctx
        awsClient = AwsClient()
        mTransferUtility = awsClient!!.getTransferUtility(mContext!!)
    }


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun doUpload(mFile: File) {
        if(Util.isOnline(mContext!!)){
            GlobalScope.launch(Dispatchers.Main) {

                try {
                    mTransferObserver = mTransferUtility!!.upload(
                        Const.AWS_BUCKET_NAME,
                        ObjectSingleton.userLogin?.user_id + "_" + mFile.name,
                        mFile,
                        CannedAccessControlList.PublicRead
                    )
                    Log.d("mTransferListen","AWS File Uploading doUpload:")
                    mTransferObserver!!.setTransferListener(mTransferListen)
                } catch (e: Exception) {
                    awsPhotoUploadView?.onAwsError()
                    awsUploadFileListener?.onAwsError()
                    Log.d("mTransferListen","AWS File Uploading doUpload: ${e.message}")
                }
            }
        } else {
            awsPhotoUploadView?.onAwsError()
            awsUploadFileListener?.onAwsError()
        }
    }

    var mTransferListen: TransferListener = object : TransferListener{
        override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
            Log.d("mTransferListen","AWS File Uploading bytesCurrent :$bytesCurrent")
            Log.d("mTransferListen","AWS File Uploading bytesTotal :$bytesTotal")
        }

        override fun onStateChanged(id: Int, state: TransferState?) {
            if (state == TransferState.COMPLETED) {
                val url =
                    "https://" + Const.AWS_BUCKET_NAME + ".s3.amazonaws.com/" + mTransferObserver!!.key
                Log.d("mTransferListen","AWS File Uploading completed URL :$url")
                awsPhotoUploadView?.onSuccessfullyUploaded(url)
                awsUploadFileListener?.onSuccessfullyUploaded(url)
            }
        }

        override fun onError(id: Int, ex: Exception?) {
            ex?.printStackTrace()
            awsPhotoUploadView?.onAwsError()
            awsUploadFileListener?.onAwsError()
            Log.d("mTransferListen","AWS File Uploading onError :")
        }

    }
}