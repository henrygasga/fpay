package com.fortunepay.requestModel

data class SemiTwoDetails(
    var company_id: String,
    var source_of_income: String,
    var nature_of_work_id: String,
    var nature_of_work: String,
    var company_name: String,
    var document_id: String,
    var document_number: String,
    var document_image: String
)