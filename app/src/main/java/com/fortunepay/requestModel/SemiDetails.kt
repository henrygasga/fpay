package com.fortunepay.requestModel

data class SemiDetails(
    var company_id: String,
    var address: String,
    var city: String,
    var state: String,
    var nationality: String
)