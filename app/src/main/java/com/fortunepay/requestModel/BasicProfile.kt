package com.fortunepay.requestModel

data class BasicProfile(
    var company_id: String,
    var first_name: String,
    var middle_name: String,
    var last_name: String,
    var email: String,
    var birth_date: String,
    var location_lat: String,
    var location_lng: String,
    var image: String? = null
)