package com.fortunepay.requestModel

data class OtpModel(
    val deviceInfo: String,
    val deviceType: String,
    val deviceId: String,
    val companyId: String,
    val otp: String,
    val token: String,
    val resendType: Int? = null
)