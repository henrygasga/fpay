package com.fortunepay.requestModel

import retrofit2.http.Field

data class LoginModel(
    var dialCode: String,
    var phoneNumber: String,
    var deviceInfo: String,
    var deviceType: String,
    var deviceId: String,
    var companyId: String,
    var sendOtp: String? = null,
    var appVersion: String? = null,
    var osVersion: String? = null,
    var manufacturer: String? = null
)