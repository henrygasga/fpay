package com.fortunepay.views.fragments

import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fortunepay.R
import com.fortunepay.adapters.GenericAdapter
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.ItemClickInterface
import com.fortunepay.model.NotificationData
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*

class NotificationFragment : BaseFragment() {
    private var items: ArrayList<NotificationData> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBackButton.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.HOME)
        }

        tv_pagetitle.text = getString(R.string.notification_title)

        for (i in 1..20){
            items.add(
                NotificationData(
                    "$i",
                    "Notification $i",
                    "34 Mon 2019 5:09 PM"
                )
            )
        }

        rvNotificationList.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(activity, ClipDrawable.HORIZONTAL))
            adapter = GenericAdapter(items, R.layout.item_notification, com.fortunepay.BR.model,
                object : ItemClickInterface<NotificationData>{
                    override fun onItemClick(v: View?, item: NotificationData, position: Int) {

                    }

                })
        }
    }
}