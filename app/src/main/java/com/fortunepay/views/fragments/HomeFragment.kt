package com.fortunepay.views.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.WalletInterface
import com.fortunepay.model.WalletData
import com.fortunepay.presenter.WalletPresenter
import com.fortunepay.utils.Const
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_profile_header.*

class HomeFragment : BaseFragment(), WalletInterface.View {

    var walletPresenter: WalletPresenter? = null

    override fun onBalanceSuccess(wallet: WalletData) {
        tvMyBalance.text = wallet.wallet_balance.toString()
    }

    override fun onBalanceFailed(msg: String) {
        showErrorMsg(msg)
    }

    override fun onResume() {
        super.onResume()
        walletPresenter?.getBalance(Const.COMPANY_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(activity) {
            is DashboardActivity -> {
                thisActivity = (activity as DashboardActivity)
            }
        }

        walletPresenter = WalletPresenter(thisActivity)
        walletPresenter!!.viewBallance = this
        updateInfo()

        ivMenu.setOnClickListener {
            (activity as DashboardActivity).dlHome.openDrawer(GravityCompat.END)
        }
        ivPhotoDisplay.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }
        llUserInfo.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }
        linHideBalance.setOnClickListener {
            linHideBalance.visibility = View.INVISIBLE
            tvMyBalance.visibility = View.GONE
            llShowBalance.visibility = View.VISIBLE
        }
        llShowBalance.setOnClickListener {
            linHideBalance.visibility = View.VISIBLE
            tvMyBalance.visibility = View.VISIBLE
            llShowBalance.visibility = View.GONE
        }
        llscanqr.setOnClickListener {
            (activity as DashboardActivity).scanToPay()
        }
    }

    fun updateInfo(){
//        tvUserName.text = ObjectSingleton.userLogin?.first_name
//        tvBusinessName.text = " ${ObjectSingleton.userLogin?.last_name}"
        Log.d("UPDATEINFO","${(activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_FIRST_NAME, "")}")
        Log.d("UPDATEINFO","${(activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_LAST_NAME, "")}")
        tvUserName.text = (activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_FIRST_NAME, "")
        tvBusinessName.text = " " + (activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_LAST_NAME, "")
        tvMobile.text = (activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_DIAL_CODE, "") + " " + (activity as DashboardActivity).os!!.getPrefByKey(Const.KEY_USER_PHONE, "")
    }
}