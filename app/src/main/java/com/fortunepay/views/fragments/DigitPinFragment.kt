package com.fortunepay.views.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.core.widget.doAfterTextChanged
import com.fortunepay.R
import com.fortunepay.api.ApiFactory
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.SingletonManager
import com.fortunepay.views.activities.LoginActivity
import com.fortunepay.views.activities.RegisterActivity
import com.fortunepay.views.activities.VerifyPinActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_digit_pin.*
import kotlinx.android.synthetic.main.otp_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class DigitPinFragment : BaseFragment(), AccountInterface.PinVerify {

    override fun onVerified(body: ResponseData) {
        dialogLoading!!.dismiss()

        os.token = body.token
        when(activity) {
            is LoginActivity -> {
                om?.saveUser(body.user)
                (activity as LoginActivity).goToDashboard()
            }
            is RegisterActivity -> {
                (activity as RegisterActivity).goToSlide(3)
            }
        }
    }

    override fun onFailedVerify(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg)
    }

    var accountPresenter: AccountPresenter? = null
    var dialogLoading: AppCompatDialog? = null
    var loginPin: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_digit_pin,container,false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(activity) {
            is LoginActivity ->  {
                thisActivity = (activity as LoginActivity)
            }
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        om = SingletonManager.getInstance(thisActivity)
        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewPinVerify = this
        dialogLoading = DialogFactory.createProgressDialog(thisActivity)
        setLabels()

        tvChange.setOnClickListener {
            (activity as LoginActivity).goToSlide(0)
        }

        mtvone.doAfterTextChanged {
            mtvtwo.requestFocus()
        }
        mtvtwo.doAfterTextChanged {
            mtvthree.requestFocus()
        }
        mtvthree.doAfterTextChanged {
            mtvfour.requestFocus()
        }
        mtvfour.doAfterTextChanged {
//            dialogLoading!!.show()
        }

        tvReg.setOnClickListener{
            loginPin = mtvone.text.toString() + mtvtwo.text.toString() + mtvthree.text.toString() + mtvfour.text.toString()
            validateRequest()
        }
    }

    fun validateRequest() {
        dialogLoading!!.show()

        if(loginPin != null && loginPin!!.length == 4){
            when(activity) {
                is LoginActivity -> {
                    accountPresenter!!.verifyPin(
                        Const.COMPANY_ID, loginPin!!,
                        om!!.getPrefByKey(Const.KEY_USER_DIAL_CODE, "")!!,
                        om!!.getPrefByKey(Const.KEY_USER_PHONE, "")!!
                    )
                }
                is RegisterActivity -> {
                    accountPresenter!!.createPin(
                        Const.COMPANY_ID,
                        loginPin!!,
                        "Android",
                        "A"
                        )
                }
            }
        } else {
            dialogLoading!!.dismiss()
            showErrorMsg("Please enter PIN to proceed.")
        }
    }

    fun setLabels() {
        when(activity) {
            is LoginActivity ->  {
                tvReg.text = getString(R.string.btn_verify)
                mtv_enter_verify.text = getString(R.string.enter_pin)
                llChangeMobile.visibility = View.VISIBLE
                tvMobileNum.text = om!!.getPrefByKey(Const.KEY_USER_DIAL_CODE, "") + om!!.getPrefByKey(Const.KEY_USER_PHONE, "")
            }
            is RegisterActivity -> {
                tvReg.text = getString(R.string.btn_register)
                llMerchantDetails.visibility = View.GONE
                tvForgotPin.visibility = View.GONE
            }
            is VerifyPinActivity -> {
                mtv_enter_verify.text = getString(R.string.enter_pin)
            }
        }
    }
}