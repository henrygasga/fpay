package com.fortunepay.views.fragments

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.fragment_save_cards.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*

class SaveCardsFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_save_cards,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initActionsOnClickEvents()

        tv_pagetitle.text = getString(R.string.save_banks)
        mcvBpi.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.SAVE_ACCOUNTS_PAGE)
        }

    }
}