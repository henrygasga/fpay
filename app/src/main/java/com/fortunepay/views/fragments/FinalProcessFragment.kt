package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.views.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_final_process.*

class FinalProcessFragment : BaseFragment(), AccountInterface.AccountFinalProcessView {

    var dialogLoading: AppCompatDialog? = null
    var accountPresenter: AccountPresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_final_process,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(activity) {
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        accountPresenter = AccountPresenter(mActivity)
        accountPresenter!!.viewFinal = this
        dialogLoading = DialogFactory.createProgressDialog(mActivity)

        btnLaterClick.setOnClickListener {
            (activity as RegisterActivity).goToDashboard()
        }
        btnSubmitNow.setOnClickListener {
            dialogLoading!!.show()
            validate()
        }
    }

    fun validate() {
        if(mtvViberContact.text.toString().isEmpty() ||
            mtvWeChatContact.text.toString().isEmpty()) {
            dialogLoading!!.dismiss()
            showErrorMsg(getString(R.string.video_call_validation))
        } else {
            accountPresenter!!.accountFinalProcess(
                Const.COMPANY_ID,
                mtvWeChatContact.text.toString(),
                mtvViberContact.text.toString()
            )
        }
    }

    override fun onFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    override fun onFinalSuccess(baseResponse: ResponseData) {
        dialogLoading!!.dismiss()
        om?.saveUser(baseResponse.user)
        os.token = baseResponse.token

        (activity as RegisterActivity).goToSlide(11)
    }
}