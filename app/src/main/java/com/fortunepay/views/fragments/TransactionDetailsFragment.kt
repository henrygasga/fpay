package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.model.TransactionData
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*
import kotlinx.android.synthetic.main.transaction_toolbar.*

class TransactionDetailsFragment : BaseFragment() {

    companion object {
        fun getInstance(transData: TransactionData): TransactionDetailsFragment {
            val fragment = TransactionDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable("transData", transData)
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transaction_details,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivMenu.setOnClickListener {
            (activity as DashboardActivity).dlHome.openDrawer(GravityCompat.END)
        }
        ivPhotoDisplay.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }
        llUserInfo.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }

        ivBackButton.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.TRANSACTION_HISTORY)
        }

        tv_pagetitle.text = getString(R.string.transaction_header)

        init()
    }

    fun init() {
        if(arguments != null) {
            val transData : TransactionData? = arguments!!.getParcelable("transData")
            val title: String? = arguments!!.getString("title")
            val message: String? = arguments!!.getString("message")

            trans_toolbar.title = transData!!.txn_id
//            iv.setImageResource(resid)
//            tvTitle.text = title
//            tvMessage.text = message
        }
    }
}