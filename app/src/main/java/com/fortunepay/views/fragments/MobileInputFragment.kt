package com.fortunepay.views.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.interfaces.DialogClickInterface
import com.fortunepay.model.BaseResponse
import com.fortunepay.model.User
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.LoginModel
import com.fortunepay.utils.*
import com.fortunepay.views.activities.LoginActivity
import com.fortunepay.views.activities.RegisterActivity
import com.fortunepay.views.activities.SelectionActivity
import kotlinx.android.synthetic.main.fragment_mobile_input.*

class MobileInputFragment : BaseFragment(), AccountInterface.View, AccountInterface.phoneRegister {

    var accountPresenter: AccountPresenter? = null
    var dialogLoading: AppCompatDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobile_input,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(activity) {
            is LoginActivity -> {
                thisActivity = (activity as LoginActivity)
                tvLogin.text = (activity as LoginActivity).getString(R.string.btn_register)
            }
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
                tvLogin.text = (activity as RegisterActivity).getString(R.string.btn_login)
            }
        }

        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewAccount = this
        accountPresenter!!.viewRegister = this
        dialogLoading = DialogFactory.createProgressDialog(thisActivity)
        om = SingletonManager.getInstance(thisActivity)

        tvNext.setOnClickListener {
            validateLogin()
        }

        tvLogin.setOnClickListener {
            goToNext()
        }

    }

    fun goToNext() {
        when(activity) {
            is LoginActivity -> {
                (activity as LoginActivity).goToRegister()
            }
            is RegisterActivity -> {
                (activity as RegisterActivity).goToLogin()
            }
        }
    }

    fun validateLogin() {
        if(mtv_mobilenumber.text!!.trim().toString().isEmpty()) {
            showErrorMsg(getString(R.string.input_mobile_number))
        } else if(mtv_mobilenumber.text!!.trim().toString().length < 10) {
            showErrorMsg(getString(R.string.not_valid_number))
        } else {
            when(activity) {
                is LoginActivity -> {
                    val loginModel = LoginModel(
                        "+63",
                        mtv_mobilenumber.text.toString(),
                        "Android",
                        "A",
                        "1234",
                        Const.COMPANY_ID,
                        "Y"
                    )

                    accountPresenter!!.login(loginModel)
                    dialogLoading!!.show()
                }
                is RegisterActivity -> {
                    val loginModel = LoginModel(
                        "+63",
                        mtv_mobilenumber.text.toString(),
                        "Android",
                        "A",
                        "1234",
                        Const.COMPANY_ID
                    )
                    accountPresenter!!.phoneRegister(loginModel)
                    dialogLoading!!.show()
                }
            }

        }
    }

    // AccountInterface.View

    override fun loginSuccess(baseResponse: BaseResponse) {
        os.userLogin = baseResponse.data.user
        om?.saveUser(baseResponse.data.user)
        os.token = baseResponse.data.token
        dialogLoading!!.dismiss()
        mtv_mobilenumber.text!!.clear()
        (activity as LoginActivity).goToSlide(1)
    }

    override fun loginFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    // AccountInterface.phoneRegister

    override fun onRegisterFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    override fun onRegisterSuccess(baseResponse: BaseResponse) {
        os.userLogin = baseResponse.data.user
//        om?.saveUser(baseResponse.data.user)
        os.token = baseResponse.data.token
        dialogLoading!!.dismiss()
        mtv_mobilenumber.text!!.clear()

        (activity as RegisterActivity).goToSlide(1)
    }
}