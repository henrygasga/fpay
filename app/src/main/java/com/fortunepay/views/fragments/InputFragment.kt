package com.fortunepay.views.fragments

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.Fragment
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.interfaces.ScreenChangeListener
import com.fortunepay.interfaces.WalletInterface
import com.fortunepay.model.ScanByUserId
import com.fortunepay.model.User
import com.fortunepay.model.WalletData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.presenter.WalletPresenter
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.fragment_input.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*
import org.json.JSONObject

class InputFragment : BaseFragment(), AccountInterface.scanById, ScreenChangeListener, WalletInterface.View {

    var amount: Double = 0.0
    var dialogLoading: AppCompatDialog? = null
    var accountPresenter: AccountPresenter? = null
    var walletPresenter: WalletPresenter? = null

    companion object {
        fun getInstance(qrData: String): InputFragment {
            val fragment = InputFragment()
            val bundle = Bundle()
            bundle.putString("qrData", qrData)
            fragment.arguments = bundle
            return fragment
        }

    }

    fun init() {
        if(arguments != null) {
            dialogLoading!!.show()
            val qrData: String? = arguments!!.getString("qrData")

            val jsonObj = JSONObject(qrData!!)
            val user_id: String = jsonObj.getString("user_id")

            accountPresenter!!.scanQRByUserId(
                Const.COMPANY_ID,
                user_id
            )
        } else {
            (activity as DashboardActivity).supportFragmentManager.beginTransaction().remove(this).commit()
            (activity as DashboardActivity).changeContent(ScreenType.HOME)
        }

    }

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg)
    }

    override fun onSuccess(act: User) {
        dialogLoading!!.dismiss()
        os.userTransaction = act
        tvMerhName.text = act.first_name + " " + act.last_name
        tvAddress.text = act.dial_code + " " + act.phone_number

    }

    override fun onChange(type: ActivityType) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_input,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(activity) {
            is DashboardActivity -> {
//                tvLogin.visibility = View.GONE
                thisActivity = (activity as DashboardActivity)

            }
        }

        dialogLoading = DialogFactory.createProgressDialog(thisActivity)
        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewScanById = this
        walletPresenter = WalletPresenter(thisActivity)
        walletPresenter!!.viewBallance = this
        init()

        ivBackButton.setOnClickListener {
            (activity as DashboardActivity).onBackPressed()
        }
        tv_pagetitle.text = getString(R.string.scan_to_pay)


        btn100.setOnClickListener {
            metAmount.setText("100")
        }
        btn200.setOnClickListener {
            metAmount.setText("200")
        }
        btn500.setOnClickListener {
            metAmount.setText("500")
        }
        btn1000.setOnClickListener {
            metAmount.setText("1000")
        }
        btn2000.setOnClickListener {
            metAmount.setText("2000")
        }
        btn5000.setOnClickListener {
            metAmount.setText("5000")
        }

        tvNext.setOnClickListener {
            validateRequest()
        }

    }

    fun validateRequest() {
        val amt = metAmount.text
        amount = java.lang.Double.parseDouble(amt.toString())
        if(!amt.isNullOrEmpty()){
            if(amount > 0){
                showErrorMsg("Please enter valid amount")
            } else {
                dialogLoading!!.show()
                walletPresenter?.getBalance(Const.COMPANY_ID)
            }
        } else {
            showErrorMsg("Please input valid amount.")
        }
    }

    override fun onBalanceSuccess(wallet: WalletData) {
        if(java.lang.Double.parseDouble(wallet.wallet_balance.toString()) >
            amount) {

        } else {
            showErrorMsg("Insuficient Balance")
        }
    }

    override fun onBalanceFailed(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg)
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}