package com.fortunepay.views.fragments

import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.view.*
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fortunepay.R
import com.fortunepay.adapters.GenericAdapter
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.ItemClickInterface
import com.fortunepay.model.TransactionData
import com.fortunepay.utils.ScreenType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_transaction_history.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*
import kotlinx.android.synthetic.main.transaction_toolbar.*

class TransactionHistoryFragment : BaseFragment() {
    private var items: ArrayList<TransactionData> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_transaction_history,container,false)
        setHasOptionsMenu(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).setSupportActionBar(trans_toolbar)

        ivMenu.setOnClickListener {
            (activity as DashboardActivity).dlHome.openDrawer(GravityCompat.END)
        }
        ivPhotoDisplay.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }
        llUserInfo.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.PROFILE)
        }

        ivBackButton.setOnClickListener {
            (activity as DashboardActivity).changeContent(ScreenType.HOME)
        }

        tv_pagetitle.text = getString(R.string.transaction_header)

        for (i in 1..20){
            items.add(
                TransactionData(
                    "Transactin Number $i",
                    "8875683535835638",
                    "8955"
                )
            )
        }

        rvTransactionList.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = GenericAdapter(items, R.layout.item_transaction, com.fortunepay.BR.model,
                object : ItemClickInterface<TransactionData> {
                    override fun onItemClick(v: View?, item: TransactionData, position: Int) {
                        (activity as DashboardActivity).transactionDetails = item
                        (activity as DashboardActivity).changeContent(ScreenType.TRANSACTION_DETAILS)
                    }
                })
            addItemDecoration(DividerItemDecoration(activity, HORIZONTAL))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.transaction_search_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}