package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.views.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_final_step.*

class FinalStepFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_final_step,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnStartNow.setOnClickListener {
            (activity as RegisterActivity).goToSlide(10)
        }

        btnLater.setOnClickListener {
            (activity as RegisterActivity).goToDashboard()
        }
    }
}