package com.fortunepay.views.fragments

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.core.widget.doAfterTextChanged
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.interfaces.ScreenChangeListener
import com.fortunepay.model.BaseResendOtp
import com.fortunepay.model.BaseResponse
import com.fortunepay.model.OtpResend
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.OtpModel
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.views.activities.LoginActivity
import com.fortunepay.views.activities.RegisterActivity
import com.stfalcon.smsverifycatcher.OnSmsCatchListener
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher
import kotlinx.android.synthetic.main.fragment_enter_otp.*
import kotlinx.android.synthetic.main.fragment_mobile_input.*
import kotlinx.android.synthetic.main.otp_view.*
import java.util.*
import java.util.regex.Pattern

class EnterOtpFragment : BaseFragment(), AccountInterface.OtpSignIn, ScreenChangeListener {
    override fun otpResend(baseOtp: BaseResendOtp) {
        os.token = baseOtp.data.token
        val msg: String = baseOtp.data.message
//        showErrorMsg(msg)
    }

    override fun onChange(type: ActivityType) {
        Log.d("ActivityType","ActivityType = $type")
        activityType = type
        runTimer()
    }

    override fun onReceived(baseResponse: BaseResponse, act: ActivityType) {

        os.token = baseResponse.data.token
        dialogLoading!!.dismiss()

        countDownTimer!!.cancel()

        mtvone.text!!.clear()
        mtvtwo.text!!.clear()
        mtvthree.text!!.clear()
        mtvfour.text!!.clear()

        when(act) {
            ActivityType.LOGIN -> {
                os.userLogin = baseResponse.data.user
                (activity as LoginActivity).goToSlide(2)
            }
            ActivityType.REGISTER -> {
                (activity as RegisterActivity).goToSlide(2)
            }
            else -> {
                showErrorMsg(getString(R.string.something_wong))
            }
        }
    }

    override fun verifyFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    var dialogLoading: AppCompatDialog? = null
    var countDownTimer: CountDownTimer? = null
    var otpText: String? = null
    var accountPresenter: AccountPresenter? = null
    var activityType: ActivityType? = null
    var smsVerifyCatcher: SmsVerifyCatcher? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_enter_otp,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(activity) {
            is LoginActivity -> {
//                tvLogin.visibility = View.GONE
                thisActivity = (activity as LoginActivity)
                tvRegister.text = getString(R.string.btn_next)
                (activity as LoginActivity).setListener(this)
            }
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
                tvRegister.text = getString(R.string.btn_next)
                (activity as RegisterActivity).setListener(this)
            }
        }

        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewOtpVerify = this
        dialogLoading = DialogFactory.createProgressDialog(thisActivity)

        mtvone.doAfterTextChanged {
            mtvtwo.requestFocus()
        }
        mtvtwo.doAfterTextChanged {
            mtvthree.requestFocus()
        }
        mtvthree.doAfterTextChanged {
            mtvfour.requestFocus()
        }
        mtvfour.doAfterTextChanged {
//            dialogLoading!!.show()
        }

        tvRegister.setOnClickListener {
            validateRequest()
        }

        smsVerifyCatcher = SmsVerifyCatcher(thisActivity,this,
            OnSmsCatchListener<String> { message ->
                val code = parseCode(message!!)
                Toast.makeText(thisActivity, "code = $code", Toast.LENGTH_LONG).show()
                mtvone.setText(code.toCharArray()[0].toString())
                mtvtwo.setText(code.toCharArray()[1].toString())
                mtvthree.setText(code.toCharArray()[2].toString())
                mtvfour.setText(code.toCharArray()[3].toString())
                validateRequest()
            })
    }

    private fun parseCode(message: String): String {
        val p = Pattern.compile("\\b\\d{4}\\b")
        val m = p.matcher(message)
        var code = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    override fun onStart() {
        super.onStart()
        smsVerifyCatcher!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher!!.onStop()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        smsVerifyCatcher!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun validateRequest() {
        otpText = mtvone.text.toString() + mtvtwo.text.toString() + mtvthree.text.toString() + mtvfour.text.toString()
        dialogLoading!!.show()
        if(otpText != null && otpText!!.length == 4) {
            val otpModel = OtpModel(
                "Android",
                "A",
                "1234",
                Const.COMPANY_ID,
                otpText!!,
                os.token!!.token
            )

            when(activityType) {
                ActivityType.LOGIN -> {
                    accountPresenter!!.verifyOtp(otpModel, ActivityType.LOGIN)
                }
                ActivityType.REGISTER -> {
                    accountPresenter!!.verifyOtp(otpModel, ActivityType.REGISTER)
                }
                else -> {
                    dialogLoading!!.dismiss()
                    showErrorMsg(getString(R.string.something_wong))
                }
            }

        } else {
            dialogLoading!!.dismiss()
            showErrorMsg("Please enter OTP ro proceed.")
        }
    }

    fun resendOtp() {
        accountPresenter!!.resentOtp(3, Const.COMPANY_ID, os.token!!.token)
        runTimer()
    }

    fun runTimer() {
        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val remainingSeconds = millisUntilFinished / 1000

                mtvResendCode.text = String.format(Locale.ENGLISH, getString(R.string.resend_code_in), remainingSeconds)
            }

            override fun onFinish() {
                mtvResendCode.text = getString(R.string.resend_code)
                mtvResendCode.setOnClickListener {
                    resendOtp()
                }
            }
        }.start()
    }
}