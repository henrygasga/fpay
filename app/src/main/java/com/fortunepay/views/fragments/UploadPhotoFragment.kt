package com.fortunepay.views.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialog
import com.bumptech.glide.Glide
import com.fortunepay.R
import com.fortunepay.aws.AwsUploadFileTask
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.interfaces.AwsInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.SemiTwoDetails
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.Util
import com.fortunepay.utils.easyphotopicker.DefaultCallback
import com.fortunepay.utils.easyphotopicker.EasyImage
import com.fortunepay.views.activities.DisplayPhotoActivity
import com.fortunepay.views.activities.RegisterActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import id.zelory.compressor.Compressor
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_upload_photo.*
import java.io.File
import java.io.IOException

class UploadPhotoFragment : BaseFragment(), AwsInterface.View, AccountInterface.SemiThreeDetailsView {


    var awsPresenter: AwsUploadFileTask? = null
    var accountPresenter: AccountPresenter? = null
    var dialogLoading: AppCompatDialog? = null
    var mPhotoFile: File? = null
    val SUCCESS: Int = 1
    val CANCEL: Int = 0
//    val CANCEL: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_upload_photo,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(activity) {
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        awsPresenter = AwsUploadFileTask(thisActivity,this)
        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewSemiThreeDetails = this
        dialogLoading = DialogFactory.createProgressDialog(thisActivity)

        ivPhoto.setOnClickListener {
            loadPhotoActivity()
        }
        tvUploadPhoto.setOnClickListener {
            if(mPhotoFile == null) {
                loadPhotoActivity()
            } else {
                dialogLoading!!.show()
                awsPresenter?.doUpload(mPhotoFile!!)
            }
        }
    }

    fun loadPhotoActivity() {
        val intent = Intent(mActivity, DisplayPhotoActivity::class.java)
        intent.putExtra(Const.SELECTION_TYPE, Const.DISPLAY_PHOTO_FILE)
        startActivityForResult(intent, Const.DISPLAY_PHOTO_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == Const.DISPLAY_PHOTO_FILE) {
            if(resultCode == SUCCESS) {
                onPhotosReturned()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun onPhotosReturned() {
//        Logger.Error("Path : " + returnedPhotos[0].absolutePath)
        Toast.makeText(mActivity, "REturned", Toast.LENGTH_LONG).show()
        mPhotoFile = ObjectSingleton.displayPicFile
        try {
            mPhotoFile = Compressor(thisActivity).compressToFile(mPhotoFile)
            Util.loadProfileImage(Glide.with(this), mPhotoFile!!, ivPhoto)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg)
    }

    override fun onSemiThreeDetailsSuccess(baseResponse: ResponseData) {
        dialogLoading!!.dismiss()
        om?.saveUser(baseResponse.user)
        os.token = baseResponse.token

        (activity as RegisterActivity).goToSlide(8)
    }

    override fun onSuccessfullyUploaded(url: String) {
//        dialogLoading!!.dismiss()
        accountPresenter!!.semiThreeDetails(
            SemiTwoDetails(
                Const.COMPANY_ID,
                "",
                "",
                "",
                "",
                ObjectSingleton.identificationCard!!.id,
                "",
                ""
            ),
            url
        )
    }

    override fun onAwsError() {
        dialogLoading!!.dismiss()
        showErrorMsg("Error Uploading.")
    }
}