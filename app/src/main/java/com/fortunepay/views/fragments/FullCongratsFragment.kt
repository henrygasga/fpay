package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.views.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_congrats_full.*

class FullCongratsFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_congrats_full,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnStartFull.setOnClickListener {
            (activity as RegisterActivity).goToDashboard()
        }
    }
}