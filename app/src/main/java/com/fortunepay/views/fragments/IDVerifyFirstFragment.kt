package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.SemiDetails
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.views.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_id_verify_1.*

class IDVerifyFirstFragment : BaseFragment(), AccountInterface.SemiDetailsView {

    var dialogLoading: AppCompatDialog? = null
    var accountPresenter: AccountPresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_id_verify_1,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(activity) {
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        accountPresenter = AccountPresenter(mActivity)
        accountPresenter!!.viewSemiDetails = this
        dialogLoading = DialogFactory.createProgressDialog(mActivity)

        tvNext.setOnClickListener {
            dialogLoading!!.show()
            validateRequest()
        }

        btn_skip.setOnClickListener {
            (activity as RegisterActivity).goToDashboard()
        }
    }

    fun validateRequest() {
        if(mtv_address1.text.toString().isEmpty() ||
            mtv_city.text.toString().isEmpty() ||
            mtv_state.text.toString().isEmpty() ||
            mtv_nationality.text.toString().isEmpty()) {
            dialogLoading!!.dismiss()
            showErrorMsg("All Fields required.")
        } else {
            accountPresenter!!.semiDetails(
                SemiDetails(
                    Const.COMPANY_ID,
                    mtv_address1.text.toString(),
                    mtv_city.text.toString(),
                    mtv_state.text.toString(),
                    mtv_nationality.text.toString()
                )
            )
        }
    }

    override fun onFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    override fun onSemiDetailsSuccess(baseResponse: ResponseData) {
        dialogLoading!!.dismiss()
        om?.saveUser(baseResponse.user)
        os.token = baseResponse.token

        (activity as RegisterActivity).goToSlide(6)
    }
}