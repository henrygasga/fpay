package com.fortunepay.views.fragments

import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fortunepay.R
import com.fortunepay.adapters.GenericAdapter
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.ItemClickInterface
import com.fortunepay.model.SaveAccountsData
import com.fortunepay.utils.ActivityType
import com.fortunepay.views.activities.DashboardActivity
import kotlinx.android.synthetic.main.fragment_save_accounts.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*
import kotlinx.android.synthetic.main.transaction_toolbar.*

class SaveAccountsPageFragment : BaseFragment() {
    private var items: ArrayList<SaveAccountsData> = arrayListOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_save_accounts,container,false)
        setHasOptionsMenu(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as DashboardActivity).setSupportActionBar(trans_toolbar)
        initActionsOnClickEvents()
        tv_pagetitle.text = getString(R.string.save_banks)
        trans_toolbar.title = getString(R.string.save_accounts)

        for (i in 1..2){
            items.add(
                SaveAccountsData(
                    "Transactin Number $i",
                    "SAVINGS",
                    "**** **** 688$i"
                )
            )
        }

        rvSaveAccountList.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = GenericAdapter(items,R.layout.item_save_accounts, com.fortunepay.BR.model,
                object : ItemClickInterface<SaveAccountsData>{
                    override fun onItemClick(v: View?, item: SaveAccountsData, position: Int) {

                    }

                })
            addItemDecoration(DividerItemDecoration(activity, HORIZONTAL))
        }

        ivAddNewAccount.setOnClickListener {
            val thisActivity = activity as DashboardActivity
            thisActivity.launchActivity(thisActivity, ActivityType.BANK_LOGIN)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.transaction_search_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}