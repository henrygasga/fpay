package com.fortunepay.views.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.BasicProfile
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.Util
import com.fortunepay.views.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_initial_details.*
import java.util.*

class InitialDetailsFragment : BaseFragment(), AccountInterface.BasicProfileRegistration, DatePickerDialog.OnDateSetListener {

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        dob = calendar.timeInMillis
        mtv_bday.setText(Util.getDateToShow(dob!!, Util.DATE_FORMAT_DAY_MONTH_YEAR))
    }

    override fun onBasicFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    override fun onBasicSuccess(baseResponse: ResponseData) {
        dialogLoading!!.dismiss()
        om?.saveUser(baseResponse.user)
        os.token = baseResponse.token

//        Log.d("onBasicSuccess","$")
        when(activity) {
            is RegisterActivity -> {
                (activity as RegisterActivity).goToSlide(4)
            }
        }
    }

    var dialogLoading: AppCompatDialog? = null
    var accountPresenter: AccountPresenter? = null
    var dob: Long? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_initial_details,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(activity) {
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        accountPresenter = AccountPresenter(thisActivity)
        accountPresenter!!.viewBasic = this
        dialogLoading = DialogFactory.createProgressDialog(thisActivity)

        tvSave.setOnClickListener {
            dialogLoading!!.show()
            validateRequest("")
        }
        mtv_bday.setOnClickListener {
            showDatePicker()
        }
        tl_bday.setOnClickListener {
            showDatePicker()
        }
    }

    fun validateRequest(imgUrl: String) {
        if(mtv_fname.text.toString().isEmpty() ||
            mtv_mname.text.toString().isEmpty() ||
            mtv_lname.text.toString().isEmpty() ||
            mtv_email.text.toString().isEmpty() ||
            mtv_bday.text.toString().isEmpty()) {
            dialogLoading!!.dismiss()
            showErrorMsg("All Fields required.")
        } else {
            accountPresenter?.basicProfile(
                BasicProfile(
                    Const.COMPANY_ID,
                    mtv_fname.text.toString(),
                    mtv_mname.text.toString(),
                    mtv_lname.text.toString(),
                    mtv_email.text.toString(),
                    dob!!.div(1000).toString(),
                    "1","1",
                    imgUrl
                )
            )
        }
    }

    fun showDatePicker() {
        val calendar = Calendar.getInstance()
        if (dob != null) {
            calendar.setTimeInMillis(dob!!)
        }
        val that = activity as RegisterActivity
        val thisListener = this

        val datePickerDialog = DatePickerDialog(
            that,
            thisListener,
            calendar
                .get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }
}