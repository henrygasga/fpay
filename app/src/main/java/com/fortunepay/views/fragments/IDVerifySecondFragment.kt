package com.fortunepay.views.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.model.NatureOfWork
import com.fortunepay.model.ResponseData
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.requestModel.SemiTwoDetails
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.SelectionType
import com.fortunepay.views.activities.PhotoUploadActivity
import com.fortunepay.views.activities.RegisterActivity
import com.fortunepay.views.activities.SelectionActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_id_verify_2.*

class IDVerifySecondFragment : BaseFragment(), AccountInterface.SemiTwoDetailsView {

    override fun onFailed(msg: String) {
        showErrorMsg(msg, dialogLoading)
    }

    override fun onSemiTwoDetailsSuccess(baseResponse: ResponseData) {
        dialogLoading!!.dismiss()
        om?.saveUser(baseResponse.user)
        os.token = baseResponse.token

        (activity as RegisterActivity).goToSlide(7)
    }

    var dialogLoading: AppCompatDialog? = null
    var accountPresenter: AccountPresenter? = null
    val SUCCESS: Int = 1
    var selectedNatureOfWork: NatureOfWork? = null
    var selectedIdentification: NatureOfWork? = null
    var photoUrl: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_id_verify_2,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(activity) {
            is RegisterActivity -> {
                thisActivity = (activity as RegisterActivity)
            }
        }

        accountPresenter = AccountPresenter(mActivity)
        accountPresenter!!.viewSemiTwoDetails = this
        dialogLoading = DialogFactory.createProgressDialog(mActivity)

        tvNext.setOnClickListener {
            validateRequest()
        }

        btnskip.setOnClickListener {
            (activity as RegisterActivity).goToDashboard()
        }

        etwork.setOnClickListener {
            ObjectSingleton.getSelection = SelectionType.NATURE_OF_WORK
            val intent = Intent(mActivity, SelectionActivity::class.java)
            intent.putExtra(Const.SELECTION_TYPE, Const.NATURE_OF_WORK)
            startActivityForResult(intent, Const.NATURE_OF_WORK)
        }

        etcard.setOnClickListener {
            ObjectSingleton.getSelection = SelectionType.IDENTIFICATION
            val intent = Intent(mActivity, SelectionActivity::class.java)
            intent.putExtra(Const.SELECTION_TYPE, Const.IDENTIFICATION)
            startActivityForResult(intent, Const.IDENTIFICATION)
        }

        etphotoip.setOnClickListener {
            val intent = Intent(mActivity, PhotoUploadActivity::class.java)
            intent.putExtra(Const.SELECTION_TYPE, Const.PHOTO_FILE_URL)
            startActivityForResult(intent, Const.PHOTO_FILE_URL)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == Const.NATURE_OF_WORK) {
            if(resultCode == SUCCESS) {
                val model = data!!.getStringExtra(Const.SELECTED_DATA)
                selectedNatureOfWork = Gson().fromJson(model, NatureOfWork::class.java)
                etwork.setText(selectedNatureOfWork?.title)
            }
        } else if(requestCode == Const.IDENTIFICATION) {
            if(resultCode == SUCCESS) {
                val model = data!!.getStringExtra(Const.SELECTED_DATA)
                selectedIdentification = Gson().fromJson(model, NatureOfWork::class.java)
                ObjectSingleton.identificationCard = selectedIdentification
                etcard.setText(selectedIdentification?.title)
            }
        } else if(requestCode == Const.PHOTO_FILE_URL) {
            if(resultCode == SUCCESS) {
                val fileUrl = data!!.getStringExtra(Const.FILE_PATH)
                photoUrl = fileUrl
                etphotoip.setText("Document ID Uploaded")
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun validateRequest() {

        if(etsource.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_source_of_income))
        } else if(etwork.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_nature_of_work))
        } else if(etcompanyname.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_company_business_name))
        } else if(etcard.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_identification_card))
        } else if(etidnumber.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_id_number))
        } else if(etphotoip.text.toString().isEmpty()) {
            showErrorMsg(getString(R.string.validation_id_photo))
        } else {
            dialogLoading!!.show()
            accountPresenter!!.semiTwoDetails(
                SemiTwoDetails(
                    Const.COMPANY_ID,
                    etsource.text.toString(),
                    selectedNatureOfWork!!.id,
                    etwork.text.toString(),
                    etcompanyname.text.toString(),
                    selectedIdentification!!.id,
                    etidnumber.text.toString(),
                    photoUrl!!
                )
            )
        }
    }
}