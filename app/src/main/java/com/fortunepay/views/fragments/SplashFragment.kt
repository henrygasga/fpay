package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import com.fortunepay.views.activities.SplashActivity
import kotlinx.android.synthetic.main.fragment_splash.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashFragment : BaseFragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext
    get() = Dispatchers.Main + Job()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        animation_view.imageAssetsFolder = "images/"
        animation_view.setAnimation("newsplash.json")
        animation_view.loop(false)
        animation_view.playAnimation()
        launch {
            delay(3000)
            withContext(Dispatchers.Main) {
                (activity as SplashActivity).moveToLogin()
            }
        }
    }

//    GlobalScope
}
