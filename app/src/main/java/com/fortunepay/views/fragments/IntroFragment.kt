package com.fortunepay.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fortunepay.R
import com.fortunepay.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_intro.*

class IntroFragment : BaseFragment() {

    companion object {
        fun getInstance(resId: Int, title: String, message: String): IntroFragment {
            val fragment = IntroFragment()
            val bundle = Bundle()
            bundle.putInt("resId", resId)
            bundle.putString("title", title)
            bundle.putString("message", message)
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_intro,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init() {
        if(arguments != null) {
            val resid : Int = arguments!!.getInt("resId")
            val title: String? = arguments!!.getString("title")
            val message: String? = arguments!!.getString("message")

            iv.setImageResource(resid)
            tvTitle.text = title
            tvMessage.text = message
        }
    }

}