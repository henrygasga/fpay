package com.fortunepay.views.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.utils.Const
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.SingletonHolder
import com.fortunepay.utils.SingletonManager

class SplashActivity : BaseActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        os = SingletonManager.getInstance(baseContext)
    }

    fun moveToLogin() {
        var intent = Intent(this, IntroActivity::class.java)
        if(os!!.getPrefByKey(Const.KEY_INTRO, true)) {
            os!!.setPrefByKey(Const.KEY_INTRO, false)
        } else {
            intent = Intent(this, LoginActivity::class.java)
        }

        startActivity(intent)
        finish()
    }
}