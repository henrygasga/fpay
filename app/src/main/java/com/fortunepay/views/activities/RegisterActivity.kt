package com.fortunepay.views.activities

import android.content.Intent
import android.os.Bundle
import com.fortunepay.R
import com.fortunepay.adapters.IntroPagerAdapter
import com.fortunepay.base.BaseActivity
import com.fortunepay.interfaces.ScreenChangeListener
import com.fortunepay.utils.ActivityType
import com.fortunepay.views.fragments.*
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.activity_intro.vp
import kotlinx.android.synthetic.main.activity_login.*

class RegisterActivity : BaseActivity() {

    private var introAdapter: IntroPagerAdapter? = null
    var changeScreenListener: ScreenChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        introAdapter = IntroPagerAdapter(supportFragmentManager)
        introAdapter!!.addFragment(MobileInputFragment()) // 0
        introAdapter!!.addFragment(EnterOtpFragment())// 1
        introAdapter!!.addFragment(DigitPinFragment())// 2
        introAdapter!!.addFragment(InitialDetailsFragment())// 3
        introAdapter!!.addFragment(InitialRegisterSuccess())// 4
        introAdapter!!.addFragment(IDVerifyFirstFragment())// 5
        introAdapter!!.addFragment(IDVerifySecondFragment())// 6
        introAdapter!!.addFragment(UploadPhotoFragment())// 7
        introAdapter!!.addFragment(SemiVerifiedFragment())// 8
        introAdapter!!.addFragment(FinalStepFragment()) // 9
        introAdapter!!.addFragment(FinalProcessFragment()) // 10
        introAdapter!!.addFragment(FullCongratsFragment()) // 11
        vp.adapter = introAdapter

    }

    fun goToDashboard() {
        launchActivity(this, ActivityType.DASHBOARD)
        finish()
    }

    fun goToLogin() {
        launchActivity(this, ActivityType.LOGIN)
        finish()
    }

    fun setListener(listener: ScreenChangeListener) {
        this.changeScreenListener = listener
    }

    fun goToSlide(pageNum: Int) {
        vp.setCurrentItem(pageNum, true)
        when(pageNum) {
            0 -> {}
            1 -> {
                tv_pagetitle.text = getString(R.string.btn_register)
                changeScreenListener?.onChange(ActivityType.REGISTER)
            }
            2 -> {}
            3 -> {

            }
            4 -> {}
            5 -> {}
            6 -> {}
            7 -> {
                tv_pagetitle.text = getString(R.string.id_verification)
            }
            8 -> {}
            9 -> {}
            10 -> {}
        }
    }
}