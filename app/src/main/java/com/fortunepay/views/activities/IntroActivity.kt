package com.fortunepay.views.activities

import android.content.Intent
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import com.fortunepay.R
import com.fortunepay.adapters.IntroPagerAdapter
import com.fortunepay.base.BaseActivity
import com.fortunepay.views.fragments.IntroFragment
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : BaseActivity() {
    private var introAdapter: IntroPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        introAdapter = IntroPagerAdapter(supportFragmentManager)
        introAdapter!!.addFragment(getIntroFragment(R.drawable.ic_fp_intro, 0))
        introAdapter!!.addFragment(getIntroFragment(R.drawable.ic_vector_scan, 1))
        introAdapter!!.addFragment(getIntroFragment(R.drawable.ic_vector_bills_payment, 2))
        introAdapter!!.addFragment(getIntroFragment(R.drawable.ic_vector_transfer, 3))
        vp.adapter = introAdapter
        indicator.setViewPager(vp)

        tvRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        tvLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun getIntroFragment(@DrawableRes resId: Int,msgNum: Int): IntroFragment {
        return IntroFragment.getInstance(
            resId, inroTitle[msgNum],
            introMsg[msgNum]
        )
    }

    val inroTitle: ArrayList<String> = arrayListOf(
        "Welcome To Fortune Pay",
        "Scan to Pay",
        "Bills Payment",
        "P2P Transfer"
    )

    val introMsg: ArrayList<String> = arrayListOf(
        "Simply scan the QR code in store or online to pay.\n" +
                " A smarter and simpler way for shopping.",
        "Simply scan the QR code in store or online to pay. A smarter and simpler way for shopping.",
        "Enable customers of service providers to enjoy hassle-free bills payment anytime anywhere.",
        "Users to enjoy the fastest and secure way of instant money -transfer with peers, generally completed in just a few seconds."
    )
}