package com.fortunepay.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fortunepay.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
