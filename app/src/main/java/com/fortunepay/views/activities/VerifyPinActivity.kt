package com.fortunepay.views.activities

import android.os.Bundle
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.views.fragments.DigitPinFragment
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*

class VerifyPinActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_pin)

        tv_pagetitle.text = getString(R.string.confirm_title)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_layout_content, DigitPinFragment())
            addToBackStack(null)
            commit()
        }
    }
}