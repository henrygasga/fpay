package com.fortunepay.views.activities

import android.os.Bundle
import android.webkit.WebViewClient
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.ScreenType
import kotlinx.android.synthetic.main.activity_bank_login_webview.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*

class BankLoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_login_webview)

        tv_pagetitle.text = "BPI Login"

        ivBackButton.setOnClickListener {
            launchActivity(this,ActivityType.DASHBOARD)
        }

        wbBankLogin.webViewClient = WebViewClient()
        wbBankLogin.loadUrl("https://testoauth.bpi.com.ph/bpi/api/oauth2/authorize?response_type=code&client_id=005270d1-228e-4393-a36a-c53090803668&redirect_uri=https://devapi.fortunepay.com.ph/v1/bankauthorize/authorizebpiusersuccess&scope=fundTopUp%20transactionalAccounts")
    }
}