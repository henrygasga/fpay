package com.fortunepay.views.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import com.fortunepay.R
import com.fortunepay.adapters.IntroPagerAdapter
import com.fortunepay.base.BaseActivity
import com.fortunepay.interfaces.AccountInterface
import com.fortunepay.interfaces.ScreenChangeListener
import com.fortunepay.model.BaseResponse
import com.fortunepay.model.User
import com.fortunepay.presenter.AccountPresenter
import com.fortunepay.utils.ActivityType
import com.fortunepay.utils.Const
import com.fortunepay.utils.SingletonManager
import com.fortunepay.views.fragments.DigitPinFragment
import com.fortunepay.views.fragments.EnterOtpFragment
import com.fortunepay.views.fragments.MobileInputFragment
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.activity_intro.vp
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class LoginActivity : BaseActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private var introAdapter: IntroPagerAdapter? = null
    var changeScreenListener: ScreenChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        os = SingletonManager.getInstance(baseContext)

        introAdapter = IntroPagerAdapter(supportFragmentManager)
        introAdapter!!.addFragment(MobileInputFragment())
        introAdapter!!.addFragment(EnterOtpFragment())
        introAdapter!!.addFragment(DigitPinFragment())
        vp.adapter = introAdapter

        if(!os!!.getPrefByKey(Const.KEY_USER_DIAL_CODE, "").isNullOrEmpty() &&
                !os!!.getPrefByKey(Const.KEY_USER_PHONE, "").isNullOrEmpty()) {
            goToSlide(2)
        }

    }

    fun goToDashboard() {
        launchActivity(this, ActivityType.DASHBOARD)
        finish()
    }

    fun goToRegister() {
        launchActivity(this, ActivityType.REGISTER)
        finish()
    }

    fun setListener(listener: ScreenChangeListener) {
        this.changeScreenListener = listener
    }

    fun goToSlide(pageNum: Int) {
        vp.setCurrentItem(pageNum, true)
        when(pageNum) {
            2 -> {
                tv_pagetitle.text = getString(R.string.title_login)
            }
            1 -> {
                tv_pagetitle.text = getString(R.string.title_verify_otp)
                changeScreenListener?.onChange(ActivityType.LOGIN)
            }
            0 -> {
                tv_pagetitle.text = getString(R.string.title_login)
            }
        }
    }

}