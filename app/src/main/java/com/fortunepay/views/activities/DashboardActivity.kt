package com.fortunepay.views.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.interfaces.GenericData
import com.fortunepay.interfaces.ScreenChangeListener
import com.fortunepay.interfaces.WalletInterface
import com.fortunepay.model.TransactionData
import com.fortunepay.model.WalletData
import com.fortunepay.presenter.WalletPresenter
import com.fortunepay.utils.ScreenType
import com.fortunepay.utils.SingletonManager
import com.fortunepay.views.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import java.util.*

class DashboardActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    var removeSelectedNav: Boolean = false
    lateinit var transactionDetails: TransactionData
    lateinit var qrData: String
    var changeScreenListener: ScreenChangeListener? = null

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.navigation_tran_history -> {
                changeContent(ScreenType.TRANSACTION_HISTORY)
                return checkBotMenu()
            }
            R.id.navigation_save_cards -> {
                showSaveCards()
                return checkBotMenu()
            }
            R.id.navigation_home -> {
                changeContent(ScreenType.HOME)
                return checkBotMenu()
            }
            R.id.navigation_notifications -> {
                changeContent(ScreenType.NOTIFICATION)
                return checkBotMenu()
            }
            R.id.navigation_rewards -> {
                changeContent(ScreenType.REWARDS)
                return checkBotMenu()
            }
            R.id.nav_logout -> {
                Toast.makeText(baseContext, "Logout", Toast.LENGTH_LONG).show()
                return true
            }
        }
        return false
    }

    fun checkBotMenu(): Boolean {
        return if(removeSelectedNav){
            removeSelectedNav = false
            false
        } else {
            true
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if(count == 1){
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_dashboard)

        changeContent(ScreenType.HOME)
        nav_view.setOnNavigationItemSelectedListener(this)

        nav_view.menu.findItem(R.id.navigation_home).isChecked = true
        os = SingletonManager.getInstance(baseContext)

        val intentData = intent.getSerializableExtra("types")

        if(intentData != null) {
            when(intentData as ScreenType) {
                ScreenType.INPUT_AMOUNT -> {
                    qrData = intent.getStringExtra("qrdata")!!
                    changeContent(ScreenType.INPUT_AMOUNT)
                } else -> {
                    //do nothing
                }
            }
        }

    }

    fun changeContent(screenType: ScreenType) {
        val fragment: Fragment
        when(screenType) {
            ScreenType.HOME -> {
                fragment = HomeFragment()
            }
            ScreenType.PROFILE -> {
                fragment = ProfileFragment()
                removeSelectedNav = true
//                onNavigationItemSelected(getSelectedItem())
            }
            ScreenType.TRANSACTION_HISTORY -> {
                fragment = TransactionHistoryFragment()
            }
            ScreenType.TRANSACTION_DETAILS -> {
                fragment = TransactionDetailsFragment.getInstance(transactionDetails)
            }
            ScreenType.NOTIFICATION -> {
                fragment = NotificationFragment()
            }
            ScreenType.REWARDS -> {
                fragment = RewardsFragment()
            }
            ScreenType.SAVE_BANKS -> {
                fragment = SaveCardsFragment()
            }
            ScreenType.SAVE_ACCOUNTS_PAGE -> {
                fragment = SaveAccountsPageFragment()
            }
            ScreenType.INPUT_AMOUNT -> {
                fragment = InputFragment.getInstance(qrData)
            }
        }

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_layout_content, fragment)
            addToBackStack(screenType.toString())
            commit()
        }
    }

    fun showSaveCards(){
        val dialog1 = AppCompatDialog(this)
        dialog1.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view: View = LayoutInflater.from(this).inflate(R.layout.savecards_menu, null)
        dialog1.setContentView(view)

        //SetUpClickListener
        (view.findViewById<View>(R.id.mb_save_card)).setOnClickListener {
            dialog1.dismiss()
            changeContent(ScreenType.SAVE_BANKS)
        }

        (view.findViewById<View>(R.id.mb_save_account)).setOnClickListener {
            dialog1.dismiss()
            changeContent(ScreenType.SAVE_BANKS)
        }

        val window = dialog1.window
        val wlp = window!!.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.gravity = Gravity.BOTTOM
        wlp.verticalMargin = 0.07f
        window.attributes = wlp
        dialog1.show()
    }

    fun getSelectedItem(): MenuItem {
        val botmen = nav_view.menu
        for(i in 0..botmen.size()){
            val menItem = botmen.getItem(i)
            if(menItem.isChecked){
                return menItem
            }
        }
        return botmen.getItem(R.id.navigation_home)
    }

    fun scanToPay() {
        val intent = Intent(this, ScanQRActivity::class.java)
        startActivity(intent)
    }

    fun goToVerifyPin() {
        val intent = Intent(this, DashboardActivity::class.java)
        intent.putExtra("types", ScreenType.INPUT_AMOUNT)
//        intent.putExtra("qrdata",rawResult.text)
        startActivity(intent)
        finish()
    }


}