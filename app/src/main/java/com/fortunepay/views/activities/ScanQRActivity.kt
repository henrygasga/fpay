package com.fortunepay.views.activities

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.utils.Const
import com.fortunepay.utils.ScreenType
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_scanqr.*
import kotlinx.android.synthetic.main.layout_trans_base_toolbar.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONException
import org.json.JSONObject

class ScanQRActivity : BaseActivity(), ZXingScannerView.ResultHandler {

    var scannerView: ZXingScannerView? = null

    override fun handleResult(rawResult: Result?) {
        Log.d("QRRESULT", rawResult!!.text)

        try {

            val jsonObj = JSONObject(rawResult.text)
            val type = jsonObj.getInt("type")

            scannerView?.stopCamera()
            when(type) {
                Const.SHOW_QR_TYPE_MY_QR -> {
                    val intent = Intent(this, DashboardActivity::class.java)
                    intent.putExtra("types", ScreenType.INPUT_AMOUNT)
                    intent.putExtra("qrdata",rawResult.text)
                    startActivity(intent)
                    finish()
                }
                Const.SHOW_QR_TYPE_RECEIVE_PAYMENT -> {

                }
                else -> {
                    showErrorMsg("QR Implementation not ready")
                }
            }

        }catch (e: JSONException){
            e.printStackTrace()
            showErrorMsg("Invalid QR Data")
        }

        scannerView?.startCamera()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanqr)
        scannerView = ZXingScannerView(this)

        init()
        checkPermission()



        tv_pagetitle.text = getString(R.string.scan_to_pay)

        ivBackButton.setOnClickListener {
            finish()
        }

    }

    override fun onPause() {
        super.onPause()
        scannerView?.stopCamera()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun init() {
        val barcodeFormats = arrayListOf<BarcodeFormat>()
        barcodeFormats.add(BarcodeFormat.QR_CODE)
        scannerView!!.setFormats(barcodeFormats)
        scannerView!!.setShouldScaleToFill(true)
        scannerView!!.fitsSystemWindows = true
        flScan.addView(scannerView)

    }

    fun checkPermission() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    scannerView?.setResultHandler(this@ScanQRActivity)
                    scannerView?.startCamera()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {

                }

            }).check()
    }
}