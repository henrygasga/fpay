package com.fortunepay.views.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.fortunepay.R
import com.fortunepay.base.BaseActivity
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.Util
import com.fortunepay.utils.easyphotopicker.DefaultCallback
import com.fortunepay.utils.easyphotopicker.EasyImage
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.zelory.compressor.Compressor
import java.io.File
import java.io.IOException

class DisplayPhotoActivity : BaseActivity() {

    val SUCCESS: Int = 1
    val CANCEL: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initStoragePermission()
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        EasyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : DefaultCallback() {
                override fun onImagePickerError(e: Exception, source: EasyImage.ImageSource, type: Int) {
                    //Some error handling
                    e.printStackTrace()
                    val intent = Intent()
                    setResult(CANCEL, intent)
                    finish()
                }

                override fun onImagesPicked(
                    @NonNull imageFiles: List<File>, source: EasyImage.ImageSource,
                    type: Int
                ) {
                    Log.d("onActivityResult","onImagesPicked $imageFiles")
                    onPhotosReturned(imageFiles)
                }

                override fun onCanceled(source: EasyImage.ImageSource, type: Int) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source === EasyImage.ImageSource.CAMERA) {
                        val photoFile =
                            EasyImage.lastlyTakenButCanceledPhoto(this@DisplayPhotoActivity)
                        photoFile?.delete()
                        val intent = Intent()
                        setResult(CANCEL, intent)
                        finish()
                    }
                }
            })
    }

    private fun onPhotosReturned(returnedPhotos: List<File>) {
//        Logger.Error("Path : " + returnedPhotos[0].absolutePath)
//        mPhotoFile = returnedPhotos[0]
        ObjectSingleton.displayPicFile = returnedPhotos[0]

//        intent.putExtra(Const.FILE_PATH, url)
        val intent = Intent()
        setResult(SUCCESS, intent)
        finish()

//        try {
//            mPhotoFile = Compressor(this).compressToFile(mPhotoFile)
//            Util.loadNormalImage(Glide.with(this), mPhotoFile!!, llphotocontainer)
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }

    }

    private fun initStoragePermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        selectImageDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    showRationalDialogForPermissions()
                }
            }).check()
    }

    fun selectImageDialog() {
        val charGender =
            arrayOf<CharSequence>(getString(R.string.camera), getString(R.string.gallery))
        val alert = AlertDialog.Builder(this)
        alert.setTitle(getString(R.string.check_option))
        alert.setItems(charGender) { dialog, which ->
            if (which == 0) {
                EasyImage.openCamera(this@DisplayPhotoActivity, 0)
            } else if (which == 1) {
                EasyImage.openGallery(this@DisplayPhotoActivity, 0)
            }
        }
        alert.show()
    }

    private fun showRationalDialogForPermissions() {
        AlertDialog.Builder(this)
            .setMessage(R.string.go_permission_settings)
            .setPositiveButton(
                R.string.str_go_settings
            ) { _, _ -> openAppPermissionSettings() }
            .setNegativeButton(
                R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
                val intent = Intent()
                setResult(CANCEL, intent)
                finish()
            }.show()
    }

    private fun openAppPermissionSettings() {
        try {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }

    }
}