package com.fortunepay.views.activities

import android.content.Intent
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fortunepay.R
import com.fortunepay.adapters.GenericAdapter
import com.fortunepay.base.BaseActivity
import com.fortunepay.interfaces.DocumentInterface
import com.fortunepay.interfaces.ItemClickInterface
import com.fortunepay.model.NatureOfWork
import com.fortunepay.model.ResponseDocuList
import com.fortunepay.model.SelectionData
import com.fortunepay.presenter.DocumentPresenter
import com.fortunepay.utils.Const
import com.fortunepay.utils.DialogFactory
import com.fortunepay.utils.ObjectSingleton
import com.fortunepay.utils.SelectionType
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_selection.*
import kotlinx.android.synthetic.main.gen_toolbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlin.coroutines.CoroutineContext

class SelectionActivity : BaseActivity(), CoroutineScope, DocumentInterface.DocuList {

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
//        showErrorMsg(msg)
    }

    override fun onSuccessGetDocu(res: ResponseDocuList) {
        dialogLoading!!.dismiss()

        natureOfWorkItems = res.nature_of_work
        for (i in 0..res.nature_of_work.size) {
            items.add(
                SelectionData(
                    res.nature_of_work[i].id,
                    res.nature_of_work[i].title,
                    ""
                )
            )
            if((i+1) == (res.nature_of_work.size)) {
                renderRecyclerView()
            }
        }
    }

    fun renderRecyclerView() {
        rvList.apply {
            layoutManager = LinearLayoutManager(this@SelectionActivity)
            adapter = GenericAdapter(items, R.layout.item_selection, com.fortunepay.BR.model,
                object : ItemClickInterface<SelectionData>{
                    override fun onItemClick(v: View?, item: SelectionData, position: Int) {
                        var data: Any? = null
                        when(ObjectSingleton.getSelection) {
                            SelectionType.NATURE_OF_WORK -> {
                                data = natureOfWorkItems.find { it.id == item._id } as NatureOfWork
                            }
                            SelectionType.IDENTIFICATION -> {
                                data = natureOfWorkItems.find { it.id == item._id } as NatureOfWork
                            }
                        }

                        val datatojson = Gson().toJson(data)
                        val intent = Intent()
                        intent.putExtra(Const.SELECTED_DATA, datatojson)
                        setResult(SUCCESS, intent)
                        finish()
                    }
                })
            addItemDecoration(DividerItemDecoration(this@SelectionActivity, ClipDrawable.HORIZONTAL))
        }
    }

    var dialogLoading: AppCompatDialog? = null
    var documentPresenter: DocumentPresenter? = null
    private var natureOfWorkItems: List<NatureOfWork> = arrayListOf()
    private var items: ArrayList<SelectionData> = arrayListOf()
    val SUCCESS: Int = 1

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selection)

        documentPresenter = DocumentPresenter(this)
        documentPresenter?.viewDocuList = this
        dialogLoading = DialogFactory.createProgressDialog(this)

        setSupportActionBar(gentoolbar)
        gentoolbar.title = getString(R.string.nature_of_work)
        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        getList()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun getList() {
        dialogLoading!!.show()
        when(ObjectSingleton.getSelection) {
            SelectionType.NATURE_OF_WORK -> {
                documentPresenter?.getDocuList(Const.COMPANY_ID)
            }
            SelectionType.IDENTIFICATION -> {
                documentPresenter?.getDocuList(Const.COMPANY_ID)
            }
        }
    }
}