package com.fortunepay.model

data class BaseResendOtp(
    val success: Int,
    val error: Any,
    val data: OtpResend
)