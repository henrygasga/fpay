package com.fortunepay.model

data class ApiResponse(
    val success: Int,
    val error: Any,
    val data: ResponseData
)