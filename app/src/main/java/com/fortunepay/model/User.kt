package com.fortunepay.model

data class User(
    val id: Int?,
    val phone_number: String?,
    val dial_code: String?,
    val image: String?,
    val first_name: String?,
    val last_name: String?,
    val email: String?,
    val user_type: String?,
    val is_phone_verified: String?,
    val transaction_pin: String?,
    val login_pin: String?,
    val is_login_pin_set: String?,
    val currency: Currency?,
    val qr_code: QrCode?,
    val address: String?,
    val state: String?,
    val state_id: String?,
    val city: String?,
    val city_id: String?,
    val postal_code: String?,
    val user_id: String?,
    val activation_status: String?,
    val top_up_request_status: String?,
    val merchant_id: String?,
    val businessData: String?
)