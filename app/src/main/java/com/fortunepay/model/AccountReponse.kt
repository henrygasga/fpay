package com.fortunepay.model

data class AccountReponse(
    val success: Int,
    val error: Any,
    val data: ResponseData
)

data class ScanByUserId(
    val id: String,
    val user_id: String,
    val phone_number: String,
    val dial_code: String,
    val image: String,
    val first_name: String,
    val last_name: String,
    val email: String,
    val user_type: String,
    val is_phone_verified: String
)
