package com.fortunepay.model

data class OtpResend(
    val message: String,
    val token: Token
)