package com.fortunepay.model

data class SelectionData(
    var _id: String,
    var title: String,
    var subTitle: String
)