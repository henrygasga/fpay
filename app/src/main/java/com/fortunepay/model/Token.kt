package com.fortunepay.model

data class Token(
    val type: String,
    val token: String
)