package com.fortunepay.model

data class ResponseData(
    val user: User,
    val token: Token
)