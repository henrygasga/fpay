package com.fortunepay.model

import android.os.Parcel
import android.os.Parcelable

data class SaveAccountsData(
    var act_id: String?,
    var act_type: String?,
    var act_number: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(act_id)
        parcel.writeString(act_type)
        parcel.writeString(act_number)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SaveAccountsData> {
        override fun createFromParcel(parcel: Parcel): SaveAccountsData {
            return SaveAccountsData(parcel)
        }

        override fun newArray(size: Int): Array<SaveAccountsData?> {
            return arrayOfNulls(size)
        }
    }
}