package com.fortunepay.model


data class WalletResponse(
    val success: Int,
    val error: Any,
    val data: Wallet
)

data class Wallet(
    val message: String,
    val txn_id: String,
    val status: String,
    val status_code: String,
    val wallet: WalletData
)

data class WalletData(
    val id: String,
    val wallet_balance: Double
)