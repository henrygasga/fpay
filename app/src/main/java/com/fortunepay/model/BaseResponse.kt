package com.fortunepay.model

data class BaseResponse(
    val success: Int,
    val error: Any,
    val data: ResponseData
)