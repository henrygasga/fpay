package com.fortunepay.model

data class QrCode(
    val id: String,
    val qr_code_string: String
)