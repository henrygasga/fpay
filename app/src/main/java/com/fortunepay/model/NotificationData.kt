package com.fortunepay.model

import android.os.Parcel
import android.os.Parcelable

data class NotificationData(
    var notif_id: String?,
    var notificationTitle: String?,
    var notifData: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(notif_id)
        parcel.writeString(notificationTitle)
        parcel.writeString(notifData)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationData> {
        override fun createFromParcel(parcel: Parcel): NotificationData {
            return NotificationData(parcel)
        }

        override fun newArray(size: Int): Array<NotificationData?> {
            return arrayOfNulls(size)
        }
    }
}