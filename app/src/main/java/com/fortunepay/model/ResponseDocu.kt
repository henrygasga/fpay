package com.fortunepay.model

data class ResponseDocu(
    val success: Int,
    val error: Any,
    val data: ResponseDocuList
)

data class ResponseDocuList(
    val documents: List<Any>,
    val nature_of_work: List<NatureOfWork>
)

data class NatureOfWork(
    val description: Any,
    val id: String,
    val title: String,
    val type: Any
)