package com.fortunepay.model

data class Currency(
    val id: String,
    val name: String,
    val sysmbol: String
)